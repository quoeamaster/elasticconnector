module gitlab.com/quoeamaster/elasticconnector/v2

go 1.17

require (
	github.com/buger/jsonparser v1.1.1
	github.com/spf13/cobra v1.2.1
)

require (
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)

# include all packages (golang core as well)

# worked but all packages are covered (not just our library's package...)
#go test -coverpkg=all -coverprofile cover.out

# doesn't work somehow....
#go test -coverpkg $(go list gitlab.com/quoeamaster/aflo/...) -coverprofile cover.out

# this works super well and only our library coverage is involved
# https://www.ory.sh/golang-go-code-coverage-accurate/
# go get -u github.com/ory/go-acc
# go-acc github.com/some/package
# go-acc .
# go-acc ./...
# go-acc $(go list ./...)
# go-acc $(glide novendor)

#~/GO_src/bin/go-acc --ignore server gitlab.com/quoeamaster/aflo/...

# run the export command
source script/setEnv.sh

~/GO_src/bin/go-acc \
   gitlab.com/quoeamaster/... \
   --ignore gitlab.com/quoeamaster/elasticconnector/cmd 
   #--ignore gitlab.com/quoeamaster/elasticconnector/maild/command/credentials \
   #--ignore gitlab.com/quoeamaster/elasticconnector/server/endpoints

# run coverage report
go tool cover -html=coverage.txt

# housekeep
rm ./coverage.txt



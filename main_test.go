// Elastic Connector
// Copyright (C) 2021 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"testing"
	"time"

	"gitlab.com/quoeamaster/elasticconnector/v2/cli"
)

func Test_0_cli_export(t *testing.T) {
	//t.Skip("")
	fmt.Printf("\n** main_test.Test_0_cli_export **\n")
	if !isEnvSet() {
		t.Skip("env var is not set, skip the test; please make sure the GLASTIC_HOST etc is SET\n")
	}
	cli.ESCon.SetArgs([]string{"export", "-c", "./testdata/glastic_unit_test.json"})
	s := bytes.NewBuffer(nil)
	cli.ESCon.SetOut(s)

	if err := cli.ESCon.Execute(); err != nil {
		t.Errorf("a. error in export, reason [%v]", err)
	}
	// get the results... stdout or stderr (if any)
	if s.Len() > 0 {
		fmt.Printf("results->\n%v\n", s.String())
	}
	// verification
	if v := fileExists("./testdata/apache_demo_backup.data"); v == false {
		t.Errorf("b-1. expect testdata/apache_demo_backup.data Exists")
	}
	if v := fileExists("./testdata/apache_demo_backup-settings-mappings.data"); v == false {
		t.Errorf("b-2. expect testdata/apache_demo_backup-settings-mappings.data Exists")
	}
	if v := fileExists("./testdata/apach_demo.data"); v == false {
		t.Errorf("b-1. expect testdata/apach_demo.data Exists")
	}
}

func Test_1_cli_import(t *testing.T) {
	//t.Skip()
	fmt.Printf("\n** main_test.Test_1_cli_import **\n")
	if !isEnvSet() {
		t.Skip("env var is not set, skip the test; please make sure the GLASTIC_HOST etc is SET\n")
	}
	allPass := false
	// housekeep after import succeeded
	defer func() {
		if allPass {
			removeFile("./testdata/apach_demo.data")
			removeFile("./testdata/apach_demo-settings-mappings.data")
			removeFile("./testdata/apache_demo_backup.data")
			removeFile("./testdata/apache_demo_backup-settings-mappings.data")
		}
	}()
	// import
	s := bytes.NewBuffer(nil)
	cli.ESCon.SetOut(s)

	cli.ESCon.SetArgs([]string{"import", "-c", "./testdata/glastic_unit_test.json"})
	if err := cli.ESCon.Execute(); err != nil {
		t.Errorf("a. failed to import data, reason [%v]", err)
	}
	if s.Len() > 0 {
		fmt.Printf("result ->\n%v\n", s.String())
	}
	// verification through curl...
	if v, err := isESResourceExists("apache_demo_copy", false, true); err != nil {
		t.Errorf("b-1. expect apache_demo_copy index available, %v", err)
	} else if !v {
		t.Errorf("b-1. expect apache_demo_copy index available, somehow not exists")
	}

	if v, err := isESResourceExists("apache_demo_copy_dstream", true, true); err != nil {
		t.Errorf("b-1. expect apache_demo_copy_dstream index available, %v", err)
	} else if !v {
		t.Errorf("b-1. expect apache_demo_copy_dstream index available, somehow not exists")
	}
	allPass = true
}

func isEnvSet() bool {
	if v := os.Getenv(cli.ENV_HOST); v == "" {
		return false
	}
	if v := os.Getenv(cli.ENV_USERNAME); v == "" {
		return false
	}
	if v := os.Getenv(cli.ENV_PASSWORD); v == "" {
		return false
	}
	return true
}

func fileExists(path string) bool {
	f, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	if f.IsDir() {
		fmt.Printf("[fileExists] filename [%v] matches, but is a Directory\n", path)
		return false
	}
	return true
}

func removeFile(path string) {
	os.Remove(path)
}

// isESResourceExists - the curl results are for reference purpose;
// since ES needs a refresh rate to get docs index at a near-real time approach.
// Hence unless the curl results contain an error message; any count is valid.
func isESResourceExists(resource string, isDS bool, needHousekeep bool) (exists bool, err error) {
	hParts := strings.Split(os.Getenv(cli.ENV_HOST), "://")
	if len(hParts) != 2 {
		panic(fmt.Sprintf("failed to separate the host into '://' parts, %v", hParts))
	}
	u := os.Getenv(cli.ENV_USERNAME)
	p := os.Getenv(cli.ENV_PASSWORD)
	h := fmt.Sprintf("%v://%v:%v@%v/%v/_count", hParts[0], u, p, hParts[1], resource)
	//fmt.Printf("[deb] final url=>%v\n", h)

	s := bytes.NewBuffer(nil)
	c := exec.Cmd{
		Path:   "/usr/bin/curl",
		Args:   []string{"not-used", "-XGET", h},
		Stdout: s,
		Stderr: s,
	}
	if err2 := c.Run(); err != nil {
		err = err2
		return
	}
	// output if any
	if s.Len() > 0 {
		fmt.Printf("[result]\n%v\n", s.String())
	}
	exists = true
	// housekeep
	if needHousekeep {
		fmt.Printf("sleep for 1 seconds...")
		time.Sleep(time.Millisecond)
		s.Reset()

		if isDS {
			h = fmt.Sprintf("%v://%v:%v@%v/_data_stream/%v", hParts[0], u, p, hParts[1], resource)
		} else {
			h = fmt.Sprintf("%v://%v:%v@%v/%v", hParts[0], u, p, hParts[1], resource)
		}
		c = exec.Cmd{
			Path:   "/usr/bin/curl",
			Args:   []string{"not-used", "-XDELETE", h},
			Stdout: s,
			Stderr: s,
		}
		if err2 := c.Run(); err2 != nil {
			err = err2
			return
		}
		// output if any
		if s.Len() > 0 {
			fmt.Printf("[result - housekeep]\n%v\n", s.String())
		}
	}
	return
}

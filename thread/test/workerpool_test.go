// Elastic Connector
// Copyright (C) 2021 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package test

import (
	"fmt"
	"sync"
	"testing"

	"gitlab.com/quoeamaster/elasticconnector/v2/thread"
)

// Test_workerpool_01 - testing on the workerPool framework.
func Test_workerpool_01(t *testing.T) {
	fmt.Printf("\n** workerpool_test.Test_workerpool_01 **\n")

	pool := thread.NewWorkerPool(5)
	task := newPoolTask()
	pool.PrepareRunner(task)

	// add tasks
	for i := 0; i < 17; i++ {
		pool.AddPayload(i)
	}
	pool.Run()
	// result
	signal := <-pool.SignalChannel
	if signal == thread.SIGNAL_DONE {
		task.lock.Lock()
		fmt.Printf("data returned\n   %v\n", task.data)
		task.lock.Unlock()

	} else {
		t.Errorf("unexpected signal [%v]", signal)
	}
}

// poolTask - implementation of IWorker interface employed by the workerPool framework.
type poolTask struct {
	data map[int]bool
	lock sync.Mutex
}

// newPoolTask - contructor for poolTask
func newPoolTask() (t *poolTask) {
	t = new(poolTask)
	t.data = make(map[int]bool)
	t.lock = sync.Mutex{}
	return
}

// Do - IWorker MUST implement func.
func (p *poolTask) Do(payload ...interface{}) (err error) {
	p.lock.Lock()
	v := payload[0].(int)
	fmt.Printf("task %v... processed\n", v)
	p.data[v] = true
	p.lock.Unlock()
	return
}

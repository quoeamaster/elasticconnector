// Elastic Connector
// Copyright (C) 2021 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package thread

import (
	"bytes"
	"fmt"
	"io"
	"sync"
)

// WorkerPool - the pool of worker(s) to support mutli-thread task processing.
type WorkerPool struct {
	io.Closer

	// WorkerCount - how many worker(s) expected to support the pool.
	WorkerCount int

	// payloads - the channel for providing the workers task to work on. Task in this case is an interface object.
	// [todo] convert to generics T instead.
	payloads chan interface{}

	// payloadSlice - actual contents stored as payload
	//payloadsSlice []interface{}

	// numPayloads - the number of payloads avaiable
	numPayloads int

	// [deprecated]
	// wgroup - waitGroup object for synchronizing multi go-routines at the very end before exiting a function block.
	// Concept is go-routines = Mapping / Fanning out tasks;
	// whilst waitgroup = reducer to collect / synchronize go-routines results.
	//wgroup sync.WaitGroup

	// SignalChannel - a signal channel indicating whether the tasks are DONE or Error occured etc.
	SignalChannel chan int

	// resultChannel - a channel for error to be returned. A non-nil value means something unexpected happened.
	resultChannel chan error

	lock sync.Mutex
}

// IWorker - implementation of a worker within the pool.
type IWorker interface {
	// Do - all biz logic is located here; where payload(s) could be provided to finish the task.
	Do(payload ...interface{}) (err error)
}

// NewWorkerPool - create a new instance of [WorkerPool].
func NewWorkerPool(workerCount int) *WorkerPool {
	pool := new(WorkerPool)
	pool.WorkerCount = DEFAULT_WORKER_COUNT
	if workerCount > 0 {
		pool.WorkerCount = workerCount
	}
	// init the channels and waitgroup
	// [NOTE] by default... seems the channel only has a size of "5"... hence make it as 10000 in this case.
	pool.payloads = make(chan interface{}, 10000)
	pool.SignalChannel = make(chan int)
	pool.resultChannel = make(chan error)
	//pool.wgroup = sync.WaitGroup{}
	//pool.payloadsSlice = make([]interface{}, 0)

	return pool
}

// Close - housekeep resources before exit. This function should be called when the pool is to be diposed.
func (p *WorkerPool) Close() (err error) {
	if p.payloads != nil {
		close(p.payloads)
	}
	return
}

// PrepareRunner - start the workpool to operate until all payloads are done. Hence [Run] must be called at the very last;
// whilst [AddTask / AddPayload] should be called beforehand.
func (p *WorkerPool) PrepareRunner(taskRunner IWorker) {
	for i := 0; i < p.WorkerCount; i++ {
		k := i
		go func() {
			// create the task wrapper
			runner := newRunner(k, taskRunner)
			runner.runIWorker(p.payloads, p.resultChannel)
		}()
	}
}

// Run - Runs the task and collects the results.
func (p *WorkerPool) Run() {
	// gather results here
	p.lock.Lock()
	errs := bytes.NewBuffer(nil)
	//fmt.Printf("#payloads -> %v\n", p.numPayloads)
	for i := 0; i < p.numPayloads; i++ {
		//fmt.Printf("[debug] b4 getting err from resultChannel [%v] vs [%v]\n", i, p.numPayloads)
		err := <-p.resultChannel
		//fmt.Printf("[debug] after getting err from resultChannel\n")
		if err != nil {
			if i > 0 {
				errs.WriteString("\n")
			}
			errs.WriteString(err.Error())
		}
	}
	if len(errs.Bytes()) > 0 {
		fmt.Printf("[Run] errors involved;\n%v\n", errs.String())
	}
	// [question] closing channel should be controlled by caller instead?
	//p.Close()

	// [question] reset the payload related settings; for re-usable workerPool
	//p.numPayloads = 0
	//p.payloadsSlice = make([]interface{}, 0)

	p.lock.Unlock()

	// returns when all tasks within the workers are done.
	go func() {
		p.SignalChannel <- SIGNAL_DONE
	}()
}

// AddPayload - exactly adding a new task to the queue; the payload defines what this task requires to work it out.
func (p *WorkerPool) AddPayload(payload interface{}) {
	p.lock.Lock()

	//fmt.Printf("{deb} b4 adding to payload channel ... [%v]\n", len(payload.(string)))
	//p.payloadsSlice = append(p.payloadsSlice, payload)

	//fmt.Printf("{deb} after appending to slice ... [%v]\n", len(p.payloadsSlice))
	//p.payloads <- p.numPayloads
	p.payloads <- payload

	//fmt.Printf("{deb} b4 increment numPayloads ... [%v]\n", p.numPayloads)
	p.numPayloads = p.numPayloads + 1
	//fmt.Printf("{deb} after increment numPayloads ... [%v]\n", p.numPayloads)

	p.lock.Unlock()
}

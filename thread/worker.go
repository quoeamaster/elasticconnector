// Elastic Connector
// Copyright (C) 2021 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package thread

import (
	"fmt"
	"sync"
)

// runner - a wrapper on top of a user-provided [IWorker] implementation.
type runner struct {
	fmt.Stringer

	// id - # of this worker instance (starting from 0)
	id int

	// worker - actual Worker function to execute
	worker IWorker
}

// newRunner - create an instance of [worker] which acts as a wrapper on top of the underlying [IWorker] function.
func newRunner(id int, instance IWorker) *runner {
	w := new(runner)
	w.id = id
	w.worker = instance
	return w
}

// runIWorker - run the worker logic.
// Also an optional "lock / mutex" could be supplied if necessary.
func (w *runner) runIWorker(payloadsChannel <-chan interface{}, resultChannel chan<- error, lock ...*sync.Mutex) {
	if w.worker == nil {
		return
	}
	for p := range payloadsChannel {
		w.applyLock()
		// actual payload
		//pload := (*payloadsSlice)[p]
		// run the underlying IWorker
		err := w.worker.Do(p)
		resultChannel <- err

		w.releaseLock()
	}
}

func (w *runner) applyLock(lock ...sync.Mutex) {
	if len(lock) > 0 {
		lock[0].Lock()
	}
}
func (w *runner) releaseLock(lock ...sync.Mutex) {
	if len(lock) > 0 {
		lock[0].Unlock()
	}
}

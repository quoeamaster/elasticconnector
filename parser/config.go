// Elastic Connector
// Copyright (C) 2021 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package parser

import (
	"fmt"
	"strconv"
	"strings"

	p "github.com/buger/jsonparser"
)

// Parser - the parser instance.
type Parser struct {
	// content - config file contents in []byte.
	content []byte
}

// NewParser - create a new [Parser].
func NewParser() *Parser {
	return new(Parser)
}

// Parse - read and parse the contents of the [file].
func (c *Parser) Parse(content []byte) (err error) {
	c.content = content
	return
}

// Source - return the source []byte of the parser.
func (c *Parser) Source() []byte {
	return c.content
}

// GetValueAsString - return the associated string value based on [keys].
// The value would be interpreted as string, casting is required if the designated type is something else like int.
func (c *Parser) GetValueAsString(defaultValue string, keys ...string) (val string) {
	val, err := p.GetString(c.content, keys...)
	if err != nil {
		val = defaultValue
	}
	return
}

// GetValueAsInt64 - return the associated int64 value based on [keys].
// The return value is of type int64, further casting might be required to int or int16.
func (c *Parser) GetValueAsInt64(defaultValue int64, keys ...string) (val int64) {
	val, err := p.GetInt(c.content, keys...)
	if err != nil {
		val = defaultValue
	}
	return
}

// GetValueAsBool - return the associated boolean value based on [keys].
func (c *Parser) GetValueAsBool(defaultValue bool, keys ...string) (val bool) {
	val, err := p.GetBoolean(c.content, keys...)
	if err != nil {
		val = defaultValue
	}
	return
}

// GetValueAsRawArray - return the associated []byte value based on [keys].
func (c *Parser) GetValueAsRawArray(defaultValue []byte, keys ...string) (val []byte) {
	val, _, _, err := p.Get(c.content, keys...)
	if err != nil {
		val = defaultValue
	}
	// is it a real array?
	if len(val) > 0 {
		sVal := strings.Trim(string(val), " ")
		if sVal[0:1] != "[" || sVal[len(sVal)-1:] != "]" {
			val = defaultValue
		}
	} else if len(val) == 0 {
		val = defaultValue
	}
	return
}

// GetValueAsStringArray - return a []string based on [keys].
func (c *Parser) GetValueAsStringArray(defaultValue []string, keys ...string) (val []string) {
	bContent := c.GetValueAsRawArray(nil, keys...)
	if bContent == nil {
		return defaultValue
	}

	val = make([]string, 0)
	_, err := p.ArrayEach(bContent, func(value []byte, dataType p.ValueType, offset int, err error) {
		switch dataType {
		case p.String:
			val = append(val, string(value))
		default:
			fmt.Printf("[GetValueAsStringArray] failed to parse the value as []string, at index [%v], reason [%v]\n", offset, err)
		}
	})
	if err != nil {
		val = defaultValue
	}
	return
}

// GetValueAsArrayOfMap - return a map of string-interface{} based on the [keys].
func (c *Parser) GetValueAsArrayOfMap(defaultValue []map[string]interface{}, keys ...string) (val []map[string]interface{}) {
	bContent := c.GetValueAsRawArray(nil, keys...)
	if bContent == nil {
		return defaultValue
	}

	val = make([]map[string]interface{}, 0)
	_, err := p.ArrayEach(bContent, func(value []byte, dataType p.ValueType, offset int, err error) {
		// array of object(s)
		valMap := make(map[string]interface{})
		err3 := p.ObjectEach(value, func(key, value []byte, dataType p.ValueType, offset int) (err2 error) {
			switch dataType {
			case p.String:
				valMap[string(key)] = string(value)
			case p.Boolean:
				if bv, err := strconv.ParseBool(string(value)); err != nil {
					panic(fmt.Sprintf("[GetValueAsArrayOfMap] failed to parse the boolean value of key [%v], reason [%v]", string(key), err))
				} else {
					valMap[string(key)] = bv
				}
			// TODO: handle other data-type(s) if necessary
			default:
				fmt.Printf("[GetValueAsArrayOfMap] failed to parse the key [%v] of type [%v], of value [%v]\n", string(key), dataType.String(), string(value))
			}
			return
		})
		if err3 != nil {
			fmt.Printf("[GetValueAsArrayOfMap] failed to parse the value as []map[string]interface{} at index [%v], reason [%v]\n", offset, err3)
			return
		}
		val = append(val, valMap)
	})
	if err != nil {
		val = defaultValue
	}
	return
}

// GetRaw - return the raw value associated with the [keys], could be nil, empty []byte.
func (c *Parser) GetRaw(defaultValue []byte, keys ...string) (val []byte) {
	val, _, _, err := p.Get(c.content, keys...)
	if err != nil {
		val = defaultValue
	}
	return
}

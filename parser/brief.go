// Elastic Connector
// Copyright (C) 2021 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package parser

const (
	ES_CFG_HOST     string = "es_host"
	ES_CFG_USERNAME string = "es_username"
	ES_CFG_PASSWORD string = "es_password"

	ES_CFG_EXPORTS                                  string = "exports"
	ES_CFG_EXPORTS_BATCH_SIZE                       string = "batch_size"
	ES_CFG_EXPORTS_EXPORT_INDICES_META              string = "export_indices_meta"
	ES_CFG_EXPORTS_EXPORT_INDICES_SETTINGS_MAPPINGS string = "export_indices_settings_mappings"
	ES_CFG_EXPORTS_INDICES                          string = "indices"
	ES_CFG_EXPORTS_FILTER_QUERY                     string = "filter_query"
	ES_CFG_EXPORTS_TARGET_FOLDER                    string = "target_folder"

	ES_CFG_IMPORTS                  string = "imports"
	ES_CFG_IMPORTS_BATCH_SIZE       string = "batch_size"
	ES_CFG_IMPORTS_SOURCE_FILE      string = "source_file"
	ES_CFG_IMPORTS_SOURCE_FOLDER    string = "source_folder"
	ES_CFG_IMPORTS_USE_INDICES_META string = "use_indices_meta"

	ES_CFG_IMPORTS_CREATE_TARGET_INDICES              string = "create_target_indices"
	ES_CFG_IMPORTS_CREATE_TARGET_INDICES_TARGET_INDEX string = "target_index"
	ES_CFG_IMPORTS_CREATE_TARGET_INDICES_SOURCE_INDEX string = "source_index"
	ES_CFG_IMPORTS_CREATE_TARGET_INDICES_SOURCE_FILE  string = "source_file"
	ES_CFG_IMPORTS_CREATE_TARGET_INDICES_SCAFFOLDING  string = "scaffolding"
	ES_CFG_IMPORTS_CREATE_TARGET_INDICES_DATA_STREAM  string = "data_stream"

	ES_CFG_IMPORTS_CREATE_INDEX_DATASTREAM_IF_MISSING string = "create_index_datastream_if_missing"
)

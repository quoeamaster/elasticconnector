package test

import (
	"fmt"
	"io/ioutil"
	"testing"

	"gitlab.com/quoeamaster/elasticconnector/v2/parser"
)

// Test_parse - test parse() of the parser class.
func Test_parse(t *testing.T) {
	fmt.Printf("\n** config_test.Test_parse **\n")

	p := parser.NewParser()
	b, err := ioutil.ReadFile("./../../testdata/glastic.json")
	if err != nil {
		t.Errorf("failed to read the config file, %v", err)
	}
	if err := p.Parse(b); err != nil {
		t.Errorf("a. failed to parse the given file [], %v", err)
	}
	// get values in string, bool, int64
	if val := p.GetValueAsString("", parser.ES_CFG_HOST); val == "" {
		t.Errorf("b1. expect es_host is non empty")
	} else if val != "http://localhost:9200" {
		t.Errorf("b1. expect es_host is [http://localhost:9200], actual [%v]", val)
	}
	if val := p.GetValueAsString("", parser.ES_CFG_EXPORTS, parser.ES_CFG_EXPORTS_TARGET_FOLDER); val == "" {
		t.Errorf("b1. expect exports.target_folder is non empty")
	} else if val != "/user/exporter/data/" {
		t.Errorf("b1. expect exports.target_folder is [/user/exporter/data/], actual [%v]", val)
	}

	// bool
	if val := p.GetValueAsBool(false, parser.ES_CFG_IMPORTS, parser.ES_CFG_IMPORTS_USE_INDICES_META); !val {
		t.Errorf("b2. expect imports.use_indices_meta is [true], actual [%v]", val)
	}

	// int64
	if val := p.GetValueAsInt64(9999, parser.ES_CFG_EXPORTS, parser.ES_CFG_EXPORTS_BATCH_SIZE); val == 9999 {
		t.Errorf("b3. expect exports.batch_size is non empty")
	} else if val != 10000 {
		t.Errorf("b3. expect exports.batch_size is [10000], actual [%v]", val)
	}

	// array - raw
	if val := p.GetValueAsRawArray(nil, parser.ES_CFG_EXPORTS, parser.ES_CFG_EXPORTS_INDICES); val == nil {
		t.Errorf("b4. expect exports.indices is non empty")
	} else {
		if string(val) != `[ "class_b", "high_school_classes" ]` {
			t.Errorf("b4. expect exports.indices is [class_b, high_school_classes], actual [%v]", string(val))
		}
	}
	// array - []string
	if val := p.GetValueAsStringArray(nil, parser.ES_CFG_EXPORTS, parser.ES_CFG_EXPORTS_INDICES); val == nil {
		t.Errorf("b5. expect exports.indices is non empty")
	} else {
		if len(val) != 2 {
			t.Errorf("b5. expect exports.indices is length of 2, actual [%v]", len(val))
		}
		if !(val[0] == "class_b" && val[1] == "high_school_classes") {
			t.Errorf("b5. expect exports.indices is ['class_b', 'high_school_classes']")
		}
	}

	// map - [string]interface{}
	if val := p.GetValueAsArrayOfMap(nil, parser.ES_CFG_IMPORTS, parser.ES_CFG_IMPORTS_CREATE_TARGET_INDICES); val == nil {
		t.Errorf("b6. expect imports.create_target_indices not empty.")
	} else {
		if len(val) != 2 {
			t.Errorf("b6. expect imports.create_target_indices is of length 2, actual [%v]", len(val))
		}
		oMap := val[0]
		if v := oMap[parser.ES_CFG_IMPORTS_CREATE_TARGET_INDICES_TARGET_INDEX].(string); v != "class_b" {
			t.Errorf("b6. expect imports.create_target_indices[0].target is [class_b], actual [%v]", v)
		}
		if v := oMap[parser.ES_CFG_IMPORTS_CREATE_TARGET_INDICES_SOURCE_FILE].(string); v != "kibana_sample_data_flights" {
			t.Errorf("b6. expect imports.create_target_indices[0].source is [kibana_sample_data_flights], actual [%v]", v)
		}
		if v := oMap[parser.ES_CFG_IMPORTS_CREATE_TARGET_INDICES_SCAFFOLDING].(string); v != "./testdata/data/kibana_sample_data_flights-settings-mappings.data" {
			t.Errorf("b6. expect imports.create_target_indices[0].scaffoldings is [./testdata/data/kibana_sample_data_flights-settings-mappings.data], actual [%v]", v)
		}

		oMap = val[1]
		if v := oMap[parser.ES_CFG_IMPORTS_CREATE_TARGET_INDICES_TARGET_INDEX].(string); v != "high_school_classes" {
			t.Errorf("b6. expect imports.create_target_indices[0].target is [high_school_classes], actual [%v]", v)
		}
		if v := oMap[parser.ES_CFG_IMPORTS_CREATE_TARGET_INDICES_SOURCE_FILE].(string); v != "for_fun_client_data" {
			t.Errorf("b6. expect imports.create_target_indices[0].source is [for_fun_client_data], actual [%v]", v)
		}
		if v := oMap[parser.ES_CFG_IMPORTS_CREATE_TARGET_INDICES_SCAFFOLDING].(string); v != "./testdata/data/for_fun_client_data-settings-mappings.data" {
			t.Errorf("b6. expect imports.create_target_indices[0].scaffoldings is [./testdata/data/for_fun_client_data-settings-mappings.data], actual [%v]", v)
		}
	}

	// raw
	if val := p.GetRaw(nil, parser.ES_CFG_USERNAME); val == nil {
		t.Errorf("b7. expect username non empty")
	} else {
		if string(val) != "elastic" {
			t.Errorf("b7-1. expect username is [elastic], actual [%v]", string(val))
		}
	}

}

// Test_parse_err_case - test the error cases.
func Test_parse_err_case(t *testing.T) {
	fmt.Printf("\n** config_test.Test_parse_err_case **\n")

	p := parser.NewParser()
	b, err := ioutil.ReadFile("./../../testdata/glastic.json")
	if err != nil {
		t.Errorf("failed to read the config file, %v", err)
	}
	if err := p.Parse(b); err != nil {
		t.Errorf("a. failed to parse the given file [], %v", err)
	}
	// testing on various getXXX()
	// []map
	if val := p.GetValueAsArrayOfMap(nil, "key1", "key2"); val != nil {
		t.Errorf("b1. expect nil since keys not found, actual [%v]", val)
	}
	if val := p.GetValueAsArrayOfMap(nil, parser.ES_CFG_IMPORTS, parser.ES_CFG_IMPORTS_CREATE_TARGET_INDICES); val == nil {
		t.Errorf("b1-1. expect imports.create_target_indices non empty")
	} else {
		// should not have a value...
		oMap := val[1]
		if v := oMap["deprecated_int_key"]; v != nil {
			t.Errorf("b1-1. expect [deprecated_int_key] yields nil, actual [%v]", v)
		}
		if v := oMap[parser.ES_CFG_IMPORTS_CREATE_TARGET_INDICES_TARGET_INDEX].(string); v != "high_school_classes" {
			t.Errorf("b1-1. expect key target is [high_school_classes], actual [%v]", v)
		}
	}

	// bool
	if val := p.GetValueAsBool(false, "key1", "key2"); val != false {
		t.Errorf("b2. expect false since keys not match")
	}

	// int64
	if val := p.GetValueAsInt64(9999, parser.ES_CFG_EXPORTS, "key2"); val != 9999 {
		t.Errorf("b3. expect 9999 since keys not match")
	}

	// string
	if val := p.GetValueAsString("", "key1", "key2"); val != "" {
		t.Errorf("b4. expect '' since keys not match")
	}

	// raw array
	if val := p.GetValueAsRawArray(nil, parser.ES_CFG_EXPORTS, "key2"); val != nil {
		t.Errorf("b5. expect nil since keys not match")
	}
	if val := p.GetValueAsRawArray(nil, parser.ES_CFG_EXPORTS); val != nil {
		t.Errorf("b5-1. expect nil since keys not match, actual [%v]", string(val))
	}

	// string array
	if val := p.GetValueAsStringArray(nil, parser.ES_CFG_EXPORTS, "key2"); val != nil {
		t.Errorf("b6. expect nil since keys not match")
	}

	// raw
	if val := p.GetRaw(nil, parser.ES_CFG_USERNAME, "key2"); val != nil {
		t.Errorf("b7. expect username is empty, actual [%v]", string(val))
	}
	if val := p.GetRaw(nil, "key2"); val != nil {
		t.Errorf("b7-1. expect username is empty, actual [%v]", string(val))
	}
}

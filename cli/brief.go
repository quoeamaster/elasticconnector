// Elastic Connector
// Copyright (C) 2021 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
package cli

const (
	DEFAULT_CONFIG_FILE_LOCATION string = "./glastic.json"
	DEFAULT_WORKER_POOL_SIZE     int    = 10

	KEY_ERROR                 string = "error"
	KEY_HITS                  string = "hits"
	KEY_DATA_STREAM_TIMESTAMP string = "_data_stream_timestamp"

	KEY_PRO_DATASTREAM string = "data_stream"

	ENV_USERNAME string = "GLASTIC_USER"
	ENV_PASSWORD string = "GLASTIC_PWD"
	ENV_HOST     string = "GLASTIC_HOST"

	ERR_CODE_INDEX_DS_NOT_EXISTS string = "ERR1001"

	ERRORS_LOG string = "errors.log"
)

// Elastic Connector
// Copyright (C) 2021 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package cli

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"github.com/spf13/cobra"

	i "gitlab.com/quoeamaster/elasticconnector/v2/io"
	p "gitlab.com/quoeamaster/elasticconnector/v2/parser"
	"gitlab.com/quoeamaster/elasticconnector/v2/thread"
)

// ESCon - main CLI for the application - glastic.
var ESCon = &cobra.Command{
	Use:     "glastic",
	Short:   "glastic is a connector to elasticsearch for export and import operations",
	Long:    `glastic is a connector to elasticsearch for export and import operations`,
	Version: "1.0.0",
}

// config - config file location (defaulted to ./glastic.json)
var config string

// init - kind of ctor.
func init() {
	ESCon.PersistentFlags().StringVarP(&config, "config", "c", DEFAULT_CONFIG_FILE_LOCATION, "config file location")
	// [deprecated]
	//ESExport.Flags().BoolVarP(&exportIncludeMeta, "meta", "m", false, "include meta (_index, _type, _id) in the resulted file")

	ESCon.AddCommand(ESExport, ESImport)
}

// [deprecated] exportIncludeMeta - include meta (_index, _type, _id) during the export operation.
//var exportIncludeMeta bool

// ESExport - sub CLI for exporting data from elasticsearch.
var ESExport = &cobra.Command{
	Use:   "export",
	Short: "export data from elasticsearch",
	Long:  `export data from elasticserch`,
	RunE: func(cmd *cobra.Command, args []string) (err error) {
		// parse config
		parser := p.NewParser()
		b, err := ioutil.ReadFile(config)
		if err != nil {
			return
		}
		err = parser.Parse(b)
		if err != nil {
			return
		}
		es := prepareESStruct(parser)
		if es.Host == "" {
			return fmt.Errorf("config [es_host] is required~")
		}
		exports := prepareExportsStruct(parser)
		// need to export settings+mappings?
		if exports.ExportIndicesSettingsMappings {
			if err2 := ExportIndicesSettingsMappings(es, exports); err2 != nil {
				return err2
			}
		}
		// export indices content
		if err2 := ExportIndicesData(es, exports); err2 != nil {
			return err2
		}
		return
	},
}

// ExportIndicesData - export data from target index(s).
func ExportIndicesData(esStruct ES, exportsStruct Exports) error {
	clientP := createHttpClient(esStruct)
	for _, v := range exportsStruct.Indices {
		if err2 := executeSearchAfterForExport(esStruct, exportsStruct, v, clientP); err2 != nil {
			return err2
		}
	}
	return nil
}

// executeSearchAfterForExport - run the search_after query.
func executeSearchAfterForExport(esStruct ES, exportsStruct Exports, indexName string, clientP *http.Client) (err error) {
	// a. get the target file handle for appending
	filepath := getOutputFilepath(exportsStruct, indexName, true)
	url := fmt.Sprintf("%v/_search", indexName)
	// b. get file handle
	fileP, err := os.OpenFile(filepath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, i.PERM_READ_EXECUTE_755)
	defer func() {
		if fileP != nil {
			fileP.Close()
		}
	}()
	if err != nil {
		return
	}
	// c. build the search_after query body
	sbP := bytes.NewBuffer(nil)
	searchAfterContent := ""
	for {
		sbP.Reset()
		sbP.WriteString("{")
		// filter_query
		if exportsStruct.FilterQuery != "" {
			sbP.WriteString(fmt.Sprintf(`"query": %v, `, exportsStruct.FilterQuery))
		}
		// size
		sbP.WriteString(fmt.Sprintf(`"size": %v, `, exportsStruct.BatchSize))
		// sort
		sbP.WriteString(`"sort": [{"_doc": "desc"}]`)
		// search_after
		if searchAfterContent != "" {
			sbP.WriteString(fmt.Sprintf(`, "search_after": %v`, searchAfterContent))
		}
		sbP.WriteString(`}`)
		// [debug]
		//fmt.Printf("[debug] %v {url: %v} => query for search_after => %v\n", indexName, url, sbP.String())

		// d. execute the query (search_after with size: XXX)
		resP, err2 := runRequest(esStruct, http.MethodGet, url, sbP, clientP)
		if err2 != nil {
			return err2
		}
		// e. parse the response []byte
		parser := p.NewParser()
		parser.Parse(resP)
		if hasErr, msg := hasErrorInResponse(parser); hasErr {
			return fmt.Errorf("[executeSearchAfterForExport] response for running the query contains an error, [%v]", msg)
		}
		// e-1. is it an empty response?
		if val := parser.GetRaw(nil, KEY_HITS, KEY_HITS); val == nil {
			// all done
			break
		} else {
			sbP.Reset()

			hitArr := make([]interface{}, 0, 10000)
			json.Unmarshal(val, &hitArr)
			if len(hitArr) == 0 {
				// all done as well
				break
			}
			// build contents for file io
			for i, v2 := range hitArr {
				o := v2.(map[string]interface{})
				// update searchAfterContent (string) too...
				if i == len(hitArr)-1 {
					// [debug]
					//fmt.Printf("### [debug] %v\n", hitArr[i].(map[string]interface{}))
					finalSort := hitArr[i].(map[string]interface{})["sort"].([]interface{})
					searchAfterContent = fmt.Sprintf(`[ %v ]`, finalSort[0])
				}
				if !exportsStruct.ExportIndicesMeta {
					delete(o, "_index")
					delete(o, "_type")
					delete(o, "_id")
				}
				delete(o, "_score")
				delete(o, "sort")
				oInString, _ := json.Marshal(o)
				sbP.WriteString(fmt.Sprintf(`%v`, string(oInString)))
				sbP.WriteString("\n")
			}
			// f. write to file
			if sbP.String() != "" {
				_, err3 := fileP.WriteString(sbP.String())
				if err3 != nil {
					return err3
				}
			}
		}
	}
	return
}

// ExportIndicesSettingsMappings - export settings/mappings to file.
func ExportIndicesSettingsMappings(esStruct ES, exportsStruct Exports) error {
	// client
	clientP := createHttpClient(esStruct)
	for _, v := range exportsStruct.Indices {
		bContent, err := runRequest(esStruct, http.MethodGet, v, nil, clientP)
		if err != nil {
			return err
		}
		resParser := p.NewParser()
		resParser.Parse(bContent)
		// has error?
		if hasErr, msg := hasErrorInResponse(resParser); hasErr {
			return fmt.Errorf("[ExportIndicesSettingsMappings] failed to export settings and mappings for index [%v], reason [%v]", v, msg)
		}
		// write the definitions to the target file... [exportsStruct]
		if err2 := writeSettingsMappingsToFile(exportsStruct, v, resParser); err2 != nil {
			return fmt.Errorf("[ExportIndicesSettingsMappings] failed to write the settings/mappings to file, reason [%v]", err2)
		}
	}
	return nil
}

// hasErrorInResponse - check whether an error occured in the given parser's content.
func hasErrorInResponse(parser *p.Parser) (bool, string) {
	if v := parser.GetRaw(nil, KEY_ERROR); v != nil {
		return true, string(v)
	}
	return false, ""
}

// getOutputFilepath - prepare the correct path name for the output file.
func getOutputFilepath(exportsStruct Exports, indexName string, isDataFile bool) (v string) {
	v = exportsStruct.TargetFolder
	if v[len(v)-1:] != "/" {
		v = fmt.Sprintf("%v/%v", v, indexName)
	} else {
		v = fmt.Sprintf("%v%v", v, indexName)
	}
	// data file or settings-mappings file?
	if isDataFile {
		v = fmt.Sprintf("%v.data", v)
	} else {
		v = fmt.Sprintf("%v-settings-mappings.data", v)
	}
	return
}

// writeSettingsMappingsToFile - write the mappings + settings part to file.
func writeSettingsMappingsToFile(exportsStruct Exports, indexName string, parser *p.Parser) (err error) {
	// get the target output filepath
	f := getOutputFilepath(exportsStruct, indexName, false)

	if v := parser.GetRaw(nil, indexName); v == nil {
		// is it datastream???
		if strings.Contains(string(parser.Source()), KEY_DATA_STREAM_TIMESTAMP) {
			// write to file
			sbP := bytes.NewBufferString(fmt.Sprintf(`{"%v":`, KEY_PRO_DATASTREAM))
			sbP.WriteString(string(parser.Source()))
			sbP.WriteString(`}`)
			return i.WriteToFile(f, sbP.Bytes(), i.PERM_READ_EXECUTE_755)
		}
		return fmt.Errorf("[writeSettingsMappingsToFile] error in getting the mappings/settings section from the response on [%v], content [%v]", indexName, string(parser.Source()))
	} else {
		// write to file
		return i.WriteToFile(f, v, i.PERM_READ_EXECUTE_755)
	}
}

// ESImport - sub CLI for importing data back to elasticsearch.
var ESImport = &cobra.Command{
	Use:   "import",
	Short: "import data back to elasticsearch",
	Long:  `import data back to elasticserch`,
	RunE: func(cmd *cobra.Command, args []string) (err error) {
		// parse config
		parser := p.NewParser()
		b, err := ioutil.ReadFile(config)
		if err != nil {
			return
		}
		err = parser.Parse(b)
		if err != nil {
			return
		}
		es := prepareESStruct(parser)
		if es.Host == "" {
			return fmt.Errorf("config [es_host] is required")
		}
		imports := prepareImportsStruct(parser)
		// imports operation
		if err2 := ImportDataWithIndexCreation(&es, &imports); err2 != nil {
			err = err2
		}
		return
	},
}

func ImportDataWithIndexCreation(es *ES, imports *Imports) (err error) {
	// prepare the src_target_map (source to target indices/datastream mapping)
	mMappings, err := prepareSrcTargetMap(imports)
	if err != nil {
		return fmt.Errorf("[ImportDataWithIndexCreation] the import config has error, reason: [%v]", err)
	}
	c := createHttpClient(*es)

	for _, idxConfig := range mMappings {
		// check if the target-index is available or not; the result determines whether a missing index / datastream creation is required.
		if err2 := CheckIfIndexExists(es, idxConfig.TargetIndex, c); err2 != nil {
			// is it index not exists??
			if err2.Error() == ERR_CODE_INDEX_DS_NOT_EXISTS {
				if imports.CreateMissingIdxDS {
					// create the missing index / DS
					err = CreateIndexOrDS(es, &idxConfig, imports.SourceFolder, c)
					if err != nil {
						return
					}
				} else if !imports.CreateMissingIdxDS && idxConfig.Datastream {
					// [NOTE] for ds, the index_template MUST be created somehow; if not the data import would fail for sure.
					// create the missing DS
					err = CreateIndexOrDS(es, &idxConfig, imports.SourceFolder, c)
					if err != nil {
						return
					}
				}
				// [NOTE]
				// if create-missing-index-ds is FALSE and target-index doesn't exists;
				// then will treat it as an "INDEX" and let es guess the schema
				// (unless cluster setting has restricted dynamic index creation and return an error message by then)
			} else {
				err = err2
				return
			}
		}
		// import data
		err = ImportDataFromFile(es, imports, &idxConfig, c)
		if err != nil {
			return
		}
	}
	return
}

// ImportDataFromFile - import the data to the corresponding target-index.
func ImportDataFromFile(es *ES, imports *Imports, tImports *Targets, client *http.Client) (err error) {
	cap := int(imports.BatchSize)
	useMeta := imports.UseIndicesMeta
	dataFile := preparePathForImportFile(imports.SourceFolder, tImports.SourceFile)
	//fmt.Printf("[debug] folder, file -> [%v], [%v] -> [%v]\n", imports.SourceFolder, tImports.SourceFile, dataFile)

	fHandle, err := os.Open(dataFile)
	defer func() {
		if fHandle != nil {
			fHandle.Close()
		}
	}()
	if err != nil {
		return fmt.Errorf("[ImportDataFromFile] failed to read datafile at [%v], reason [%v]", dataFile, err)
	}
	// worker pool
	pool := thread.NewWorkerPool(thread.DEFAULT_WORKER_COUNT)
	task := NewBulkImportTask(es, imports, tImports, client)
	//fmt.Printf("[debug] b4 pool.prepareRunner [%v] - {%v}\n", tImports.TargetIndex, tImports.SourceFile)
	pool.PrepareRunner(task)
	//fmt.Printf("[debug] after pool.prepareRunner [%p]\n", &pool)

	// loop through
	s := bytes.NewBuffer(nil)
	counter := 0
	batch := 0
	scanner := i.GetLineScanner(fHandle)
	for scanner.Scan() {
		// accumulated till reaches the [cap] size and then run a bulk index / create (for datastream)
		line := scanner.Text()
		lParser := p.NewParser()
		if err2 := lParser.Parse([]byte(line)); err2 != nil {
			return fmt.Errorf("[ImportDataFromFile] failed to parse the data line [%v], reason [%v]", line, err2)
		}
		// check whether the data line matches the source index name as well... (assume multiple indices data are stored in 1 data file)
		// only apply to INDEX and not Datastream
		if !tImports.Datastream {
			if v := lParser.GetValueAsString("", "_index"); v != tImports.SourceIndex {
				continue
			}
		}
		// [datastream]
		if tImports.Datastream {
			// [note] for Datastream, the behaviour is keep on creating / adding new records; hence meta data is not useful at all...
			// logically doesn't make sense; hence, directly import the data using "create" op_type and exclude all meta.
			s.WriteString(`{"create":{}}`)
		} else {
			// [index]
			if useMeta {
				s.WriteString(fmt.Sprintf(`{"index": { "_index": "%v", "_type": "%v", "_id": "%v" }}`,
					tImports.TargetIndex,
					//lParser.GetValueAsString("non_valid_index", "_index"),
					lParser.GetValueAsString("non_valid_type", "_type"),
					lParser.GetValueAsString("non_valid_id", "_id")))
			} else {
				s.WriteString(`{ "index": {}}`)
			}
		}
		s.WriteString("\n")
		// data line (_source)
		bSrc := lParser.GetRaw([]byte(`{ "error": "non valid data line found??" }`), "_source")
		s.WriteString(string(bSrc))
		s.WriteString("\n")

		counter++
		if counter >= cap {
			batch++
			// make bulk request
			s.WriteString("\n")

			payload := s.String()
			//fmt.Printf("[debug] b4 add payload [%v]\n", tImports.TargetIndex)
			pool.AddPayload(payload)
			//fmt.Printf("[debug] after add payload\n")

			counter = 0
			s.Reset()
		}
	}
	// any leftovers?
	if counter != 0 {
		// run the bulk for the leftovers too.
		s.WriteString("\n")
		batch++

		payload := s.String()
		s.Reset()

		//fmt.Printf("[debug] b4 {remains} add payload [%v]\n", tImports.TargetIndex)
		pool.AddPayload(payload)
		//fmt.Printf("[debug] after {remains} add payload\n")
	}
	//fmt.Printf("[debug] b4 run..\n")
	pool.Run()
	//fmt.Printf("[debug] after run..\n")
	signal := <-pool.SignalChannel
	if signal == thread.SIGNAL_DONE {
		// write errors to errlog file if any
		task.lock.Lock()
		defer task.lock.Unlock()

		if task.errs.Len() > 0 {
			errLogFile := preparePathForImportFile(imports.SourceFolder, ERRORS_LOG)
			if err2 := i.WriteToFile(errLogFile, task.errs.Bytes(), i.PERM_READ_EXECUTE_755); err2 != nil {
				fmt.Printf("[runBulkImport] failed to log down the error log to [errors.log], reason [%v], content to write [%v]\n", err2, task.errs.String())
			}
		}
		pool.Close()
	} else {
		err = fmt.Errorf("[ImportDataFromFile] unknown signal situation met, [%v]", signal)
	}
	return
}

// runBulkImport - run a bulk post/create to the target index.
// Returning a bool indicating whether all documents passed the bulk operation or not.
// es *ES, index string, sourceFolder string, sbuffer *bytes.Buffer, client *http.Client) (allPass bool, err error)
func runBulkImport(es *ES, index string, sourceFolder string, payload string, client *http.Client) (allPass bool, failureReason string, err error) {
	allPass = true
	errlog := bytes.NewBuffer(nil)
	// create a io.Reader for processing
	sbuffer := strings.NewReader(payload)
	//fmt.Printf("[debug] data for bulkImport -> %v\n", payload)

	if b, err2 := runRequest(*es, http.MethodPost, fmt.Sprintf("/%v/_bulk", index), sbuffer, client); err2 != nil {
		return false, "", fmt.Errorf("[runBulkImport] failed to run bulk request on [%v], reason [%v]", index, err2)
	} else {
		rParser := p.NewParser()
		rParser.Parse(b)
		// has error?
		if hasErr, msg := hasErrorInResponse(rParser); hasErr {
			return false, "", fmt.Errorf("[runBulkImport] failed to finish the bulk request on [%v], reason [%v]", index, msg)
		}
		// has individual doc errors?
		allPass = !rParser.GetValueAsBool(true, "errors")
		// [debug]
		if !allPass {
			itemsInB := rParser.GetRaw(nil, "items")
			// search for the problematic entries and gather the error responses to "errors.log"
			if itemsInB != nil {
				itemsArr := make([]interface{}, 10000)
				if err2 := json.Unmarshal(itemsInB, &itemsArr); err2 != nil {
					return false, "", err2
				}
				for _, item := range itemsArr {
					mapItem := item.(map[string]interface{})
					// try to get "index"
					if vIndex, found := mapItem["index"]; found {
						mapIndex := vIndex.(map[string]interface{})
						if vErr, found2 := mapIndex["error"]; found2 {
							bErr, _ := json.Marshal(vErr)
							errlog.WriteString(string(bErr))
							errlog.WriteString("\n")
						}
					} else if vCreate, found := mapItem["create"]; found {
						// failed then try to get "create"
						mapCreate := vCreate.(map[string]interface{})
						if vErr, found2 := mapCreate["error"]; found2 {
							bErr, _ := json.Marshal(vErr)
							errlog.WriteString(string(bErr))
							errlog.WriteString("\n")
						}
					} // end - if index or create resultset.
				} // end - for range itemsArr
			}
			// storing errors in {{source_folder/errors.log}}
			if errlog.Len() > 0 {
				failureReason = fmt.Sprintf("[runBulkImport] failed to log down the error log to [errors.log], reason [%v]\n", err2)
			}
			//fmt.Printf("[debug] bulk respoonse -> %v\n", string(b))
		}
	}
	return
}

// CheckIfIndexExists - check if the [index] provided is accessible and has a count (could be 0 - means an empty index OR above).
func CheckIfIndexExists(es *ES, index string, client *http.Client) (err error) {
	if b, err2 := runRequest(*es, http.MethodGet, fmt.Sprintf("/%v/_count", index), nil, client); err2 != nil {
		err = err2
		return
	} else {
		rParser := p.NewParser()
		if err3 := rParser.Parse(b); err3 != nil {
			err = err3
			return
		}
		// has error?
		//fmt.Printf("[debug] %v\n", string(b))
		if hasErr, _ := hasErrorInResponse(rParser); hasErr {
			//err = fmt.Errorf("[ImportDataWithIndexCreation] has error in checking target-index existence [%v], reason [%v]", index, msg)
			err = fmt.Errorf("%v", ERR_CODE_INDEX_DS_NOT_EXISTS)
			return
		}
		// check count value
		if v := rParser.GetValueAsInt64(-1, "count"); v == -1 {
			err = fmt.Errorf("[ImportDataWithIndexCreation] expect 'count' field exists, actual content [%v]", string(rParser.Source()))
			return
		}
	}
	return
}

// CreateIndexOrDS - create the missing Datastream or Index.
func CreateIndexOrDS(es *ES, target *Targets, sourceFolder string, client *http.Client) (err error) {
	s := bytes.NewBuffer(nil)
	if target.Datastream {
		// [NOTE]
		// for datastream, the minimal requirements would be
		// a. create an index_template + data_stream: {} (or override it) -> {target_index_name}_indextemplate
		// b. add data by _bulk
		s.WriteString(fmt.Sprintf(`{"index_patterns":["%v"],"data_stream":{}}`, target.TargetIndex))
		if b, err2 := runRequest(*es, http.MethodPut, fmt.Sprintf("/_index_template/%v_indextemplate", target.TargetIndex), s, client); err2 != nil {
			return err
		} else {
			//fmt.Printf("[debug] response from index_template [%v] creation -> %v\n", fmt.Sprintf("_index_template/%v_indextemplate", target.TargetIndex), string(b))
			rParser := p.NewParser()
			rParser.Parse(b)
			// has error?
			if hasErr, msg := hasErrorInResponse(rParser); hasErr {
				return fmt.Errorf("[CreateIndexOrDS] datastream -> index_template, failed to create, reason [%v]", msg)
			}
		}
		// next is to bulk create the contents to the data_stream name; that's it... let es guess the settings/mappings though.

	} else {
		// normal index creation
		if target.Scaffolding == "" {
			// since scaffoldings file missing; let ES guess data type for the data line(s).
			fmt.Printf("[CreateIndexOrDS] scaffolding file is missing for source-index [%v]; hence skipping the index creation and let Elasticsearch to guess data type(s)\n", target.SourceIndex)
			return
		}
		// create / duplicate through the scaffolding data file's settings/mappings.
		scafile := preparePathForImportFile(sourceFolder, target.Scaffolding)
		//fmt.Printf("[debug] target file -> %v", scafile)
		mapSet := make(map[string]interface{})
		if b, err := i.ReadFile(scafile); err != nil {
			return err
		} else {
			// convert the contents to map[string]interface{}
			json.Unmarshal(b, &mapSet)
		}
		// remove non useful elements
		s.WriteString(`{"mappings":`)
		if b, err := json.Marshal(mapSet["mappings"]); err != nil {
			return err
		} else {
			s.WriteString(string(b))
		}
		// settings (remove some keys)
		sM := mapSet["settings"].(map[string]interface{})
		iM := sM["index"].(map[string]interface{})
		delete(iM, "provided_name")
		delete(iM, "creation_date")
		delete(iM, "uuid")
		delete(iM, "version")
		delete(iM, "resize")
		delete(iM, "routing")
		sM["index"] = iM
		if b, err := json.Marshal(sM); err != nil {
			return err
		} else {
			s.WriteString(fmt.Sprintf(`,"settings":%v}`, string(b)))
		}
		// execute
		if b, err := runRequest(*es, http.MethodPut, target.TargetIndex, s, client); err != nil {
			return err
		} else {
			rParser := p.NewParser()
			rParser.Parse(b)
			// has error?
			if hasErr, msg := hasErrorInResponse(rParser); hasErr {
				return fmt.Errorf("[CreateIndexOrDS] index -> failed to create, reason [%v]", msg)
			}
		}
	}
	return
}

// preparePathForImportFile - typicall connecting the [folder] and [file] values together and removing unnecessary "/" character(s).
func preparePathForImportFile(folder, file string) string {
	s := bytes.NewBuffer(nil)
	// remove trailing /
	if len(folder) > 0 && folder[len(folder)-1:] == "/" {
		s.WriteString(folder[0 : len(folder)-1])
	} else {
		s.WriteString(folder)
	}
	//fmt.Printf("[debug] %v\n", s.String())
	// add leading /
	if len(file) > 0 && file[0:1] != "/" {
		s.WriteString("/")
	}
	s.WriteString(file)
	//fmt.Printf("[debug] %v\n", s.String())
	return s.String()
}

// prepareSrcTargetMap - create the map of source-target index mappings.
func prepareSrcTargetMap(imports *Imports) (m map[string]Targets, err error) {
	m = make(map[string]Targets)

	// integrate the ImportTarget with Targets (as redundant)
	for _, v := range imports.CreateTargetIndices {
		i := Targets{
			TargetIndex: v.TargetIndex,
			SourceFile:  v.SourceFile,
			Scaffolding: v.Scaffolding,
			Datastream:  v.Datastream,
			SourceIndex: v.SourceIndex,
		}
		// validation
		if i.SourceIndex == "" || i.TargetIndex == "" {
			return nil, fmt.Errorf("source and target index MUST be provided")
		}
		if i.SourceFile == "" {
			return nil, fmt.Errorf("source DATA file MUST be provided")
		}
		m[i.SourceIndex] = i
	}
	return m, nil
}

// prepareESStruct - prepare the es connection config.
func prepareESStruct(parser *p.Parser) (s ES) {
	s = ES{
		Host:     parser.GetValueAsString("", p.ES_CFG_HOST),
		Username: parser.GetValueAsString("", p.ES_CFG_USERNAME),
		Password: parser.GetValueAsString("", p.ES_CFG_PASSWORD),
	}
	// check whether the env var contains a USERNAME or PASSWORD
	if v := os.Getenv(ENV_USERNAME); v != "" {
		s.Username = v
	}
	if v := os.Getenv(ENV_PASSWORD); v != "" {
		s.Password = v
	}
	if v := os.Getenv(ENV_HOST); v != "" {
		s.Host = v
	}
	return
}

// es - a struct encapsulating the elasticsearch connections.
type ES struct {
	Host     string
	Username string
	Password string
}

// prepareExportsStruct - prepare the exports struct
func prepareExportsStruct(parser *p.Parser) (s Exports) {
	s = Exports{
		BatchSize:                     parser.GetValueAsInt64(10000, p.ES_CFG_EXPORTS, p.ES_CFG_EXPORTS_BATCH_SIZE),
		ExportIndicesMeta:             parser.GetValueAsBool(false, p.ES_CFG_EXPORTS, p.ES_CFG_EXPORTS_EXPORT_INDICES_META),
		ExportIndicesSettingsMappings: parser.GetValueAsBool(false, p.ES_CFG_EXPORTS, p.ES_CFG_EXPORTS_EXPORT_INDICES_SETTINGS_MAPPINGS),
		Indices:                       parser.GetValueAsStringArray([]string{}, p.ES_CFG_EXPORTS, p.ES_CFG_EXPORTS_INDICES),
		FilterQuery:                   parser.GetValueAsString("", p.ES_CFG_EXPORTS, p.ES_CFG_EXPORTS_FILTER_QUERY),
		TargetFolder:                  parser.GetValueAsString("./", p.ES_CFG_EXPORTS, p.ES_CFG_EXPORTS_TARGET_FOLDER),
	}
	// limit is 10000
	if s.BatchSize > 10000 {
		s.BatchSize = 10000
	}
	return
}

// exports - a struct representing the exports config.
type Exports struct {
	BatchSize                     int64
	ExportIndicesMeta             bool
	ExportIndicesSettingsMappings bool
	Indices                       []string
	FilterQuery                   string
	TargetFolder                  string
}

// prepareImportsStruct - prepare the imports struct.
func prepareImportsStruct(parser *p.Parser) (s Imports) {
	s = Imports{
		BatchSize:          parser.GetValueAsInt64(10000, p.ES_CFG_IMPORTS, p.ES_CFG_EXPORTS_BATCH_SIZE),
		SourceFolder:       parser.GetValueAsString("./", p.ES_CFG_IMPORTS, p.ES_CFG_IMPORTS_SOURCE_FOLDER),
		UseIndicesMeta:     parser.GetValueAsBool(false, p.ES_CFG_IMPORTS, p.ES_CFG_IMPORTS_USE_INDICES_META),
		CreateMissingIdxDS: parser.GetValueAsBool(true, p.ES_CFG_IMPORTS_CREATE_INDEX_DATASTREAM_IF_MISSING),
	}
	// setup the array of createIndexTarget(s)
	oArrMap := parser.GetValueAsArrayOfMap(nil, p.ES_CFG_IMPORTS, p.ES_CFG_IMPORTS_CREATE_TARGET_INDICES)
	if oArrMap != nil {
		tArr := make([]Targets, 0)
		for _, v := range oArrMap {
			t := Targets{
				TargetIndex: v[p.ES_CFG_IMPORTS_CREATE_TARGET_INDICES_TARGET_INDEX].(string),
				SourceIndex: v[p.ES_CFG_IMPORTS_CREATE_TARGET_INDICES_SOURCE_INDEX].(string),
				SourceFile:  v[p.ES_CFG_IMPORTS_CREATE_TARGET_INDICES_SOURCE_FILE].(string),
				Datastream:  v[p.ES_CFG_IMPORTS_CREATE_TARGET_INDICES_DATA_STREAM].(bool),
			}
			// optional config, hence check the non-nil first
			if v2 := v[p.ES_CFG_IMPORTS_CREATE_TARGET_INDICES_SCAFFOLDING]; v2 != nil {
				t.Scaffolding = v2.(string)
			}
			tArr = append(tArr, t)
		}
		s.CreateTargetIndices = tArr
	}
	return
}

// imports - a struct representing the imports config.
type Imports struct {
	BatchSize           int64
	SourceFolder        string
	UseIndicesMeta      bool
	CreateTargetIndices []Targets
	CreateMissingIdxDS  bool
}

// targets - a struct representing the target import config.
type Targets struct {
	TargetIndex string
	SourceIndex string
	SourceFile  string
	Scaffolding string
	Datastream  bool
}

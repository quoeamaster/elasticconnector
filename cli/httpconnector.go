// Elastic Connector
// Copyright (C) 2021 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package cli

import (
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

// createHttpClient - create a http client for connection.
func createHttpClient(esStruct ES) *http.Client {
	c := http.DefaultClient
	c.Timeout = time.Duration(time.Duration.Minutes(1))

	return c
}

// runRequest - prepare a request and run it, returns the response content.
func runRequest(esStruct ES, method, url string, body io.Reader, client *http.Client) (val []byte, err error) {
	// [deprecated logic]
	/*
		host := esStruct.Host
		if len(host) > 0 && host[len(host)-1:] != "/" {
			host = host + "/"
		}
		host = host + url
	*/
	host := preparePathForImportFile(esStruct.Host, url)
	// [debug]
	//fmt.Printf("[debug] ### %v\n", host)

	r, err := http.NewRequest(method, host, body)
	// authentication header(s)
	if esStruct.Username != "" {
		auth := fmt.Sprintf("%v:%v", esStruct.Username, esStruct.Password)
		auth = base64.StdEncoding.EncodeToString([]byte(auth))
		auth = fmt.Sprintf("Basic %v", auth)
		r.Header.Add("Authorization", auth)
	}
	r.Header.Add("Content-Type", "application/json")

	// execute the request
	resP, err := client.Do(r)
	defer func() {
		if resP != nil {
			resP.Body.Close()
		}
	}()
	if err != nil {
		return
	}
	// read
	val, err = ioutil.ReadAll(resP.Body)
	return
}

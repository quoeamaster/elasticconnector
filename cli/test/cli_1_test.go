// Elastic Connector
// Copyright (C) 2021 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package test

import (
	"fmt"
	"os"
	"strings"
	"testing"

	"gitlab.com/quoeamaster/elasticconnector/v2/cli"
	i "gitlab.com/quoeamaster/elasticconnector/v2/io"
)

/*const (
	MSG_MISSING_CONNECTION_CONFIG string = "# missing es connection info: {host, username, password} within the config file, either fill back these info or setup env variable (e.g. GLASTIC_HOST) #"
)*/

func Test_exec(t *testing.T) {
	t.Skip()
	fmt.Printf("\n** cli_test.Test_exec **\n")
	//s := execOSCommand("/usr/bin/wc", "-l", "./../../testdata/log-basic-dev.data")
	s := execOSCommand("/usr/bin/wc", "-l", "./cli_test.go")
	fmt.Printf("[Test_exec] result -> {%v}\n", s)

}

// Test_export_settings_mappings - test exporting certain indices' settings/mappings only.
// a. setup (cli.ES, cli.Exports cli.Imports etc)
// 0. housekeep (removal of created file(s))
// b. execute the export logic (run api)
// c. verification (read back the file contents and verifiy the contents)
func Test_export_settings_mappings(t *testing.T) {
	fmt.Printf("\n** cli_test.Test_export_settings_mappings **\n")

	// a. setup
	es := cli.ES{
		Host:     os.Getenv(cli.ENV_HOST),
		Username: os.Getenv(cli.ENV_USERNAME),
		Password: os.Getenv(cli.ENV_PASSWORD),
	}
	exports := cli.Exports{
		BatchSize: 10000,
		//ExportIndicesSettingsMappings: true,
		Indices:      []string{"kibana_sample_data_ecommerce", "kibana_sample_data_flights", "log-basic-dev", "apach_demo"},
		TargetFolder: "./../../testdata/",
	}
	if !isESValidForTest(es) {
		fmt.Println(MSG_MISSING_CONNECTION_CONFIG)
		t.Skip()
	}
	// 0. housekeep
	for _, v := range exports.Indices {
		removeFile(fmt.Sprintf("./../../testdata/%v-settings-mappings.data", v))
	}
	// b. execute export settings/mappings
	if err := cli.ExportIndicesSettingsMappings(es, exports); err != nil {
		t.Errorf("a. failed to export settings/mappings, reason [%v]", err)
	}

	// c. verification
	if bContent, err := i.ReadFile(fmt.Sprintf("./../../testdata/%v-settings-mappings.data", exports.Indices[0])); err != nil {
		t.Errorf("b. failed to get the settings-mappings file [%v], reason [%v]", exports.Indices[0], err)
	} else {
		if !strings.Contains(string(bContent), "mappings\"") {
			t.Errorf("b-1. expect mappings available, content [%v]", string(bContent))
		}
	}
	if bContent, err := i.ReadFile(fmt.Sprintf("./../../testdata/%v-settings-mappings.data", exports.Indices[1])); err != nil {
		t.Errorf("b. failed to get the settings-mappings file [%v], reason [%v]", exports.Indices[1], err)
	} else {
		if !strings.Contains(string(bContent), "mappings\"") {
			t.Errorf("b-1. expect mappings available, content [%v]", string(bContent))
		}
	}
}

// Test_export_data - test on exporting data only.
// a. setup (cli.ES, cli.Exports cli.Imports etc)
// 0. housekeep (removal of created file(s))
// b. execute the export logic (run api)
// c. verification (read back the file contents and verifiy the contents)
func Test_export_data(t *testing.T) {
	fmt.Printf("\n** cli_test.Test_export_data **\n")
	// a. setup
	es := cli.ES{
		Host:     os.Getenv(cli.ENV_HOST),
		Username: os.Getenv(cli.ENV_USERNAME),
		Password: os.Getenv(cli.ENV_PASSWORD),
	}
	exports := cli.Exports{
		BatchSize:         10000,
		ExportIndicesMeta: true,
		Indices:           []string{"kibana_sample_data_ecommerce", "kibana_sample_data_flights", "kibana_sample_data_logs", "apach_demo", "log-basic-dev"},
		FilterQuery:       `{ "match_all": {}}`,
		TargetFolder:      "./../../testdata/",
	}
	if !isESValidForTest(es) {
		fmt.Println(MSG_MISSING_CONNECTION_CONFIG)
		t.Skip()
	}
	// 0. housekeep (remove previous saved data files)
	for _, v := range exports.Indices {
		removeFile(fmt.Sprintf("./../../testdata/%v.data", v))
	}

	// b. execute
	if err := cli.ExportIndicesData(es, exports); err != nil {
		t.Errorf("b. error in exporting indices data, reason [%v]", err)
	}
	// c. verification
	for _, v := range exports.Indices {
		b, err := i.ReadFile(fmt.Sprintf("./../../testdata/%v.data", v))
		if err != nil {
			t.Errorf("c. error in reading the data file, %v", err)
		}
		bString := string(b)
		bLines := strings.Split(bString, "\n")
		if len(bLines) <= 0 {
			t.Errorf("c-1. error as expected having multiple lines for [%v.data]", v)
		}
		// check whether meta data included
		if exports.ExportIndicesMeta && !strings.Contains(bString, `"_index"`) {
			t.Errorf("c-2. expect meta_data included in the exported file, but _index meta is not available anywhere, content: [%v]", bLines[0])
		}
	}
}

// Test_export_data_config_cases - test on config variataions (no meta, applied filter query).
func Test_export_data_config_cases(t *testing.T) {
	fmt.Printf("\n** cli_test.Test_export_data_config_cases **\n")
	// a. setup
	es := cli.ES{
		Host:     os.Getenv(cli.ENV_HOST),
		Username: os.Getenv(cli.ENV_USERNAME),
		Password: os.Getenv(cli.ENV_PASSWORD),
	}
	exports := cli.Exports{
		BatchSize:         10000,
		ExportIndicesMeta: false, // do not include meta
		Indices:           []string{"kibana_sample_data_ecommerce", "kibana_sample_data_flights", "apach_demo"},
		FilterQuery: `{ "bool": { "should": [
			{"match": { "DestCountry": "PL" }},
			{"match": { "customer_id": 38 }} 
		]}}`, // provide a special filter for the target indices (ecommerce should have 100 records, flights should have 405 records)
		TargetFolder: "./",
	}
	if !isESValidForTest(es) {
		fmt.Println(MSG_MISSING_CONNECTION_CONFIG)
		t.Skip()
	}
	// b. execute
	if err := cli.ExportIndicesData(es, exports); err != nil {
		t.Errorf("b. error in exporting data, [%v]", err)
	}
	// c. verification
	for _, v := range exports.Indices {
		b, err := i.ReadFile(fmt.Sprintf("./%v.data", v))
		if err != nil {
			t.Errorf("c. error in reading data file, [%v]", err)
		}

		// all cases should not have _index
		if strings.Contains(string(b), `"_index"`) {
			t.Errorf("c-0. should not involve meta data such as _index, content snippet -> %v", string(b)[0:1000])
		}
		switch v {
		case "kibana_sample_data_ecommerce":
			bLines := strings.Split(strings.Trim(string(b), "\n"), "\n")
			if len(bLines) > 100+4 {
				t.Errorf("c-1. expect ecommerce queried is 100, actual [%v]", len(bLines))
			}
			fmt.Printf("[%v] -> %v vs 100\n", v, len(bLines))
			/*
				if len(bLines) != 100 {
					fmt.Printf("[debug] inside ecommerce wc check\n")
					s := execOSCommand("/usr/bin/wc", "-l", fmt.Sprintf(`./%v.data`, v))
					sParts := strings.Split(strings.Trim(s, " "), " ")
					// get the 1st token and atoi it
					if len(sParts) > 0 {
						if i, _ := strconv.Atoi(sParts[0]); i != 100 {
							fmt.Printf("c-1. expect ecommerce queried is 100, actual [%v]\n", len(bLines))
							for _, l := range bLines {
								fmt.Println(l)
								fmt.Println()
							}
						}
					}
				}
			*/
		case "kibana_sample_data_flights":
			bLines := strings.Split(strings.Trim(string(b), "\n"), "\n")
			if len(bLines) > 405+4 {
				//if len(bLines) != 405 {
				t.Errorf("c-1. expect flights queried is 405, actual [%v]", len(bLines))
			}
			fmt.Printf("[%v] -> %v vs 405\n", v, len(bLines))
		case "apach_demo":
			bString := strings.Trim(string(b), "\n")
			if bString != "" {
				t.Errorf("c-1. expect apach_demo queried is 0, actual content...[%v]", bString)
			}
		}
	}
	// 0. housekeep
	for _, v := range exports.Indices {
		removeFile(fmt.Sprintf("./%v.data", v))
	}

	// single index query
	exports.Indices = []string{"apach_demo"}
	exports.FilterQuery = `{ "match": { "message": "internet explorer" }}`
	if err := cli.ExportIndicesData(es, exports); err != nil {
		t.Errorf("e. export apach_demo error, reason [%v]", err)
	}
	// verification
	if b, err := i.ReadFile("./apach_demo.data"); err != nil {
		t.Errorf("f. read apach_demo.data error, reason [%v]", err)
	} else {
		// count
		bString := strings.Trim(string(b), "\n")
		bLines := strings.Split(bString, "\n")
		if bString == "" {
			t.Errorf("f-1. empty content [apach_demo]??")
		}
		if len(bLines) != 34 && len(bLines) > 34+4 {
			//if len(bLines) != 34 {
			t.Errorf("f-2. expect apach_demo is 34 lines, actual [%v]", len(bLines))
		}
		fmt.Printf("[%v] -> %v vs 34\n", "apach_demo.data", len(bLines))
	}
	// housekeep
	removeFile("./apach_demo.data")

	// single index query (log-basic-dev)
	exports.Indices = []string{"log-basic-dev"}
	exports.FilterQuery = `{"match":{"msg":"fun"}}`
	if err := cli.ExportIndicesData(es, exports); err != nil {
		t.Errorf("g. export apach_demo error, reason [%v]", err)
	}
	// verification
	if b, err := i.ReadFile("./log-basic-dev.data"); err != nil {
		t.Errorf("h. read log-basic-dev.data error, reason [%v]", err)
	} else {
		// count
		bString := strings.Trim(string(b), "\n")
		bLines := strings.Split(bString, "\n")
		if bString == "" {
			t.Errorf("h-1. empty content [log-basic-dev]??")
		}
		if len(bLines) != 6 && len(bLines) > 6+4 {
			t.Errorf("h-2. expect log-basic-dev is 6 lines, actual [%v]", len(bLines))
		}
		//if len(bLines) != 4 {
		if len(bLines) > 6+4 {
			for _, l := range bLines {
				fmt.Println(l)
			}
		}
		fmt.Printf("[%v] -> %v vs 6\n", "log-basic-dev.data", len(bLines))
	}
	// housekeep
	removeFile("./log-basic-dev.data")
}

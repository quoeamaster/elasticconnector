// Elastic Connector
// Copyright (C) 2021 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package test

import (
	"fmt"
	"math/rand"
	"sync"
	"testing"
	"time"

	"gitlab.com/quoeamaster/elasticconnector/v2/thread"
)

var msgs []string = []string{
	"welcome to golang World",
	"From SanDiego to NewYork",
	"123$56&*90 vs !2#45^7*()",
}

// Test_waitgroup_0 - testing on how waitgroup works.
func Test_waitgroup_0(t *testing.T) {
	fmt.Printf("\n** waitgroup_test.Test_waitgroup_0 **\n")

	wgrp := sync.WaitGroup{}
	// non error creatable group
	for i := 0; i < 6; i++ {
		wgrp.Add(1)
		// [NOTE] - make sure the variable "i" has another reference;
		// in this case, "j"; or-else all workers would have the same "i" value
		// (i.e. the last variable value "5")
		j := i
		go func() {
			defer wgrp.Done()
			if err := workerTask(j, msgs[rand.Intn(3)]); err != nil {
				t.Logf("err: %v\n", err)
			}
		}()
	}
	wgrp.Wait()
}

// workerTask - a mimic routine task worker. If the [num] provided is "5", will throw error.
func workerTask(num int, msg string) (err error) {
	if num == 5 {
		err = fmt.Errorf("an artificial error since num is 5")
		return
	}
	time.Sleep(time.Second * time.Duration(rand.Intn(3)))
	fmt.Printf("worker [%v] - %v\n", num, msg)
	return
}

func Test_worker_pools_through_channel(t *testing.T) {
	fmt.Printf("\n** waitgroup_test.Test_worker_pools_through_channel **\n")
	// channel vs worker pool concept...
	// 3 threads / workers
	workers := 3
	wg := sync.WaitGroup{}
	msgChannel := make(chan string)

	// setup workers as a separate routine each
	for i := 0; i < workers; i++ {
		k := i
		go func() {
			if err := workerTaskChannel(k, msgChannel, wg); err != nil {
				t.Logf("error found within a worker??? [%v]", err)
			}
		}()
	}
	// randomly create n jobs (max 20, min 5)
	jobs := rand.Intn(20)
	for {
		if jobs >= 5 {
			break
		}
		jobs = rand.Intn(20)
	}
	for i := 0; i < jobs; i++ {
		//fmt.Printf("%v\n", i)
		msgChannel <- msgs[rand.Intn(3)]
	}
	close(msgChannel)

	wg.Wait()
	fmt.Printf("#%v jobs done.\n", jobs)
}

func workerTaskChannel(num int, messages <-chan string, wg sync.WaitGroup) (err error) {
	for msg := range messages {
		wg.Add(1)
		fmt.Printf("worker-pool [%v] - %v\n", num, msg)
		time.Sleep(time.Second * time.Duration(rand.Intn(3)))
		wg.Done()
	}
	return
}

// Test_workerpools - test on the WorkerPool framework. A very concise example on how the framework works;
// note on the SignalChannel handling.
func Test_workerpools(t *testing.T) {
	//t.Skip("some weird things happening in the threading framework, hence skip")
	fmt.Printf("\n** waitgroup_test.Test_workerpools **\n")

	// <the MUST ok way to run the WorkerPool>
	// create pool
	// prepareRunner (task)
	// add task....
	// run()
	// obtain signal

	// a pool of 3 workers
	pool := thread.NewWorkerPool(3)
	task := new(poolTask)
	task.dataMap = make(map[int]bool)
	pool.PrepareRunner(task)
	// prepare 20 tasks
	for i := 0; i < 20; i++ {
		k := i
		pool.AddPayload(k)
	}
	pool.Run()
	/*
		// this would become non-blocking... which is not what we want...
		select {
		case signal := <-pool.SignalChannel:
			if signal == thread.SIGNAL_DONE {
				fmt.Printf("### [debug] all tasks within the pool done\n")
				//pool.Wait() // deprecated wait() is done within the run()
				pool.Close()
			}
		default:
			t.Errorf("unknown signal situation~")
		}
	*/
	// use the blocking approach to wait for a signal instead.
	// wait till the signal is available
	fmt.Printf("$$$ [debug] b4 trying to select the signal... %v\n", time.Now())
	signal := <-pool.SignalChannel
	if signal == thread.SIGNAL_DONE {
		fmt.Printf("### [debug] all tasks within the pool done... %v\n", time.Now())
		fmt.Printf("results\n   %v\n", task.dataMap)
	}
	fmt.Printf("\n~ all DONE - last line verifying the pool is closed before displaying this message ~\n")
}

type poolTask struct {
	dataMap map[int]bool
	lock    sync.Mutex
}

func (p *poolTask) Do(payload ...interface{}) error {
	if len(payload) > 0 {
		// avoid race issue
		p.lock.Lock()
		defer p.lock.Unlock()

		num := payload[0].(int)
		fmt.Printf("processing the task #%v... %v\n", num, time.Now())
		p.dataMap[num] = true
	}
	return nil
}

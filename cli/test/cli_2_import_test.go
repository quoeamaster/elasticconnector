// Elastic Connector
// Copyright (C) 2021 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package test

import (
	"fmt"
	"net/http"
	"os"
	"testing"
	"time"

	"gitlab.com/quoeamaster/elasticconnector/v2/cli"
)

// Test_check_index_exists - checks whether the index exists or not.
// Non exists index/ds would return the error value "ERR1001".
func Test_check_index_exists(t *testing.T) {
	fmt.Printf("\n** cli_test.Test_check_index_exists **\n")
	es := cli.ES{
		Host:     os.Getenv(cli.ENV_HOST),
		Username: os.Getenv(cli.ENV_USERNAME),
		Password: os.Getenv(cli.ENV_PASSWORD),
	}
	if !isESValidForTest(es) {
		fmt.Println(MSG_MISSING_CONNECTION_CONFIG)
		t.Skip()
	}
	c := http.DefaultClient
	c.Timeout = time.Duration(time.Duration.Minutes(1))
	// run on existing index
	if err := cli.CheckIfIndexExists(&es, "kibana_sample_data_ecommerce", c); err != nil {
		t.Errorf("a. failed to run _count on existing index [kibana_sample_data_ecommerce], reason: [%v]", err)
	} else {
		t.Logf("succeed a.\n")
	}
	// run on NON existing index
	if err := cli.CheckIfIndexExists(&es, "not-exist-anyway", c); err != nil {
		if err.Error() != cli.ERR_CODE_INDEX_DS_NOT_EXISTS {
			t.Errorf("b-1. expect error to be [%v], actual [%v]", cli.ERR_CODE_INDEX_DS_NOT_EXISTS, err)
		}
		t.Logf("succeed b.\n")
	} else {
		t.Errorf("b. expect error on NON-existing index [not-exist-anyway], however the err was NIL")
	}
	// run on existing datastream
	if err := cli.CheckIfIndexExists(&es, "log-basic-dev", c); err != nil {
		t.Errorf("c. failed to run _count on existing index [log-basic-dev], reason: [%v]", err)
	} else {
		t.Logf("succeed c.\n")
	}
}

// Test_create_index_or_ds - test whether a missing index / DS could be created.
func Test_create_index_or_ds(t *testing.T) {
	fmt.Printf("\n** cli_test.Test_create_index_or_ds **\n")
	es := cli.ES{
		Host:     os.Getenv(cli.ENV_HOST),
		Username: os.Getenv(cli.ENV_USERNAME),
		Password: os.Getenv(cli.ENV_PASSWORD),
	}
	if !isESValidForTest(es) {
		fmt.Println(MSG_MISSING_CONNECTION_CONFIG)
		t.Skip()
	}
	tImports := cli.Targets{
		SourceIndex: "whatever-1",
		SourceFile:  "whatever-2",
		Scaffolding: "kibana_sample_data_flights-settings-mappings.data",
		TargetIndex: "test_create_index",
		Datastream:  false,
	}
	c := http.DefaultClient
	c.Timeout = time.Duration(time.Duration.Minutes(1))
	// test creating kibana_sample_flights scaffolding to the "test_create_index" index
	if err := cli.CreateIndexOrDS(&es, &tImports, "./../../testdata", c); err != nil {
		t.Errorf("a. failed to create [test_create_index], reason [%v]", err)
	}
	// verification
	if err := cli.CheckIfIndexExists(&es, "test_create_index", c); err != nil {
		t.Errorf("a-1. test_create_index not EXISTS? reason: [%v]", err)
	}

	tImports = cli.Targets{
		SourceIndex: "whatever-1",
		SourceFile:  "whatever-2",
		Scaffolding: "kibana_sample_data_flights-settings-mappings.data",
		TargetIndex: "test_create_ds",
		Datastream:  true,
	}
	// test creating a datastream (no mappings / settings inherited... though - for this implementation)
	if err := cli.CreateIndexOrDS(&es, &tImports, "./../../testdata", c); err != nil {
		t.Errorf("b. failed to create [test_create_ds], reason [%v]", err)
	}
	// verification - the data_stream is not created yet, instead the index_template is craeted
	/*
		if err := cli.CheckIfIndexExists(&es, "test_create_ds", c); err != nil {
			t.Errorf("b-1. test_create_ds not EXISTS? reason: [%v]", err)
		}
	*/

	// removal
	removeIndexByCurl("test_create_index", &es)
}

func Test_import_data_from_file(t *testing.T) {
	fmt.Printf("\n** cli_test.Test_import_data_from_file **\n")
	es := cli.ES{
		Host:     os.Getenv(cli.ENV_HOST),
		Username: os.Getenv(cli.ENV_USERNAME),
		Password: os.Getenv(cli.ENV_PASSWORD),
	}
	if !isESValidForTest(es) {
		fmt.Println(MSG_MISSING_CONNECTION_CONFIG)
		t.Skip()
	}
	imports := cli.Imports{
		BatchSize: 10000,
		//BatchSize:      500,
		SourceFolder:   "./../../testdata",
		UseIndicesMeta: true,
	}
	tImports := cli.Targets{
		TargetIndex: "test_create_index_1",
		SourceFile:  "kibana_sample_data_ecommerce.data",
		SourceIndex: "kibana_sample_data_ecommerce",
		Scaffolding: "kibana_sample_data_ecommerce-settings-mappings.data",
		Datastream:  false,
	}
	c := http.DefaultClient
	c.Timeout = time.Duration(time.Duration.Minutes(1))

	// index import
	if err := cli.ImportDataFromFile(&es, &imports, &tImports, c); err != nil {
		t.Errorf("a. failed to import to index [%v], reason [%v]", tImports.TargetIndex, err)
	}
	// verification
	if err := cli.CheckIfIndexExists(&es, tImports.TargetIndex, c); err != nil {
		t.Errorf("a-1. assume target index exists [%v]", tImports.TargetIndex)
	}

	// ds import
	imports = cli.Imports{
		BatchSize:      10000,
		SourceFolder:   "./../../testdata",
		UseIndicesMeta: false,
	}
	tImports = cli.Targets{
		TargetIndex: "test_create_ds",
		SourceFile:  "apach_demo_200000.data",
		SourceIndex: "apach_demo",
		Scaffolding: "apach_demo-settings-mappings.data",
		Datastream:  true,
	}
	if err := cli.ImportDataFromFile(&es, &imports, &tImports, c); err != nil {
		t.Errorf("b. failed to import to index [%v], reason [%v]", tImports.TargetIndex, err)
	}
	// verification
	if err := cli.CheckIfIndexExists(&es, tImports.TargetIndex, c); err != nil {
		t.Errorf("b-1. assume target index exists [%v]", tImports.TargetIndex)
	}

	// housekeep
	removeIndexByCurl("test_create_index_1", &es)
	removeDSByCurl("test_create_ds", &es)
}

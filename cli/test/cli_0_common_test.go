// Elastic Connector
// Copyright (C) 2021 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package test

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/quoeamaster/elasticconnector/v2/cli"
)

const (
	MSG_MISSING_CONNECTION_CONFIG string = "# missing es connection info: {host, username, password} within the config file, either fill back these info or setup env variable (e.g. GLASTIC_HOST) #"
)

// isESValidForTest - should run the test(s)?
func isESValidForTest(es cli.ES) bool {
	if es.Host == "" && es.Username == "" && es.Password == "" {
		return false
	}
	return true
}

// removeFile - remove the provided file.
func removeFile(file string) {
	if err := os.Remove(file); err != nil {
		if !strings.Contains(err.Error(), "no such file or directory") {
			panic(err)
		}
	}
}

// execOSCommand - execute the os command.
// [deprecated] as maybe due to permission issues, not possible run wc -l on an existing file??
func execOSCommand(cmd string, args ...string) string {
	sbP := bytes.NewBuffer(nil)
	finalArgs := make([]string, len(args)+1)
	finalArgs = append(finalArgs, cmd)
	finalArgs = append(finalArgs, args...)
	//fmt.Printf("[debug] final args -> %v\n", finalArgs)

	c := exec.Cmd{
		//Path: "/usr/bin/wc",
		Path: cmd,
		//Args:   []string{cmd, args[0], args[1]},
		Args:   finalArgs,
		Env:    []string{"PATH=/usr/bin"},
		Dir:    "./",
		Stdout: sbP,
		Stderr: sbP,
	}
	if err := c.Run(); err != nil {
		panic(fmt.Errorf("[execOSCommand] failed to run command [%v], reason [%v]", cmd, err))
	}
	return sbP.String()

	/*
		b, err := exec.Command(cmd, args...).Output()
		if err != nil {
			panic(fmt.Errorf("[execOSCommand] failed to run command [%v], reason [%v]", cmd, err))
		}
		return string(b)
	*/
}

// removeIndexOrDSByCurl - kind of housekeep.
func removeIndexByCurl(resource string, es *cli.ES) {
	fmt.Printf("[debug] before running curl\n")
	_ = execOSCommand("/usr/bin/curl", "-XDELETE", os.Getenv(cli.ENV_HOST)+"/"+resource, "-u", os.Getenv(cli.ENV_USERNAME)+":"+os.Getenv(cli.ENV_PASSWORD))
	//fmt.Printf("[removeIndexOrDSByCurl]\n%v\n", s)
}

func removeDSByCurl(resource string, es *cli.ES) {
	//DELETE /_data_stream/my-data-stream
	_ = execOSCommand("/usr/bin/curl", "-XDELETE", os.Getenv(cli.ENV_HOST)+"/_data_stream/"+resource, "-u", os.Getenv(cli.ENV_USERNAME)+":"+os.Getenv(cli.ENV_PASSWORD))
	//fmt.Printf("[removeIndexOrDSByCurl]\n%v\n", s)
}

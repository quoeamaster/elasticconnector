// Elastic Connector
// Copyright (C) 2021 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package test

import (
	"bytes"
	"fmt"
	"net/http"
	"os"
	"strings"
	"testing"
	"time"

	"gitlab.com/quoeamaster/elasticconnector/v2/cli"
	i "gitlab.com/quoeamaster/elasticconnector/v2/io"
)

func Test_export_index_pattern(t *testing.T) {
	fmt.Printf("\n** cli_use_cases_test.Test_export_index_pattern **\n")
	tIndex := "kibana_sample_data*"
	// a. setup
	es := cli.ES{
		Host:     os.Getenv(cli.ENV_HOST),
		Username: os.Getenv(cli.ENV_USERNAME),
		Password: os.Getenv(cli.ENV_PASSWORD),
	}
	exports := cli.Exports{
		BatchSize:         10000,
		Indices:           []string{tIndex},
		TargetFolder:      "./../../testdata/",
		ExportIndicesMeta: true,
	}
	if !isESValidForTest(es) {
		fmt.Println(MSG_MISSING_CONNECTION_CONFIG)
		t.Skip()
	}
	// export
	if err := cli.ExportIndicesData(es, exports); err != nil {
		t.Errorf("a. error in exporting index-pattern based data, reason [%v]", err)
	}
	// verification by data file #rows
	if b, err := i.ReadFile("./../../testdata/" + tIndex + ".data"); err != nil {
		t.Errorf("a-1. failed to read the data file..., [%v]", err)
	} else {
		lines := strings.Split(string(b), "\n")
		if len(lines) == 0 {
			t.Errorf("a-2. expect data file contains at least 1 row of data ..., [%v]", string(b))
		}
		fmt.Printf("lines exported is bigger than 30000, %v\n", len(lines))
	}
}

func Test_import_by_index_pattern(t *testing.T) {
	fmt.Printf("\n** cli_use_cases_test.Test_import_by_index_pattern **\n")
	// check if data file exists
	if _, err := os.Stat("./../../testdata/kibana_sample_data*.data"); os.IsNotExist(err) {
		t.Skipf("0. the data file [./../../testdata/kibana_sample_data*.data] not available, hence skip\n")
	}
	// a. setup
	es := cli.ES{
		Host:     os.Getenv(cli.ENV_HOST),
		Username: os.Getenv(cli.ENV_USERNAME),
		Password: os.Getenv(cli.ENV_PASSWORD),
	}
	if !isESValidForTest(es) {
		fmt.Println(MSG_MISSING_CONNECTION_CONFIG)
		t.Skip()
	}
	imports := cli.Imports{
		BatchSize:          10000,
		SourceFolder:       "./../../testdata",
		UseIndicesMeta:     false,
		CreateMissingIdxDS: true,
		CreateTargetIndices: []cli.Targets{
			{TargetIndex: "test_kib_flights", SourceFile: "kibana_sample_data*.data", SourceIndex: "kibana_sample_data_flights",
				Scaffolding: "kibana_sample_data_flights-settings-mappings.data", Datastream: false},
			{TargetIndex: "test_kib_ecommerce", SourceFile: "kibana_sample_data*.data", SourceIndex: "kibana_sample_data_ecommerce",
				Scaffolding: "kibana_sample_data_ecommerce-settings-mappings.data", Datastream: false},
		},
	}
	c := http.DefaultClient
	c.Timeout = time.Duration(time.Duration.Minutes(1))

	// import
	if err := cli.ImportDataWithIndexCreation(&es, &imports); err != nil {
		t.Errorf("a. failed to import??? [%v]", err)
	}
	// verification
	if err := cli.CheckIfIndexExists(&es, "test_kib_flights", c); err != nil {
		t.Errorf("a-1. test_kib_flights NOT exists? [%v]", err)
	}
	if err := cli.CheckIfIndexExists(&es, "test_kib_ecommerce", c); err != nil {
		t.Errorf("a-2. test_kib_ecommerce NOT exists? [%v]", err)
	}

	// housekeep - remove the data file too...
	removeIndexByCurl("test_kib_flights", &es)
	removeIndexByCurl("test_kib_ecommerce", &es)
	removeFile("./../../testdata/kibana_sample_data*.data")

	// roughly -> flights = 13057 rows; ecommerce = 4675 rows
}

// test without scaffoldings file (ds) ... -> exception OR let es guess
// [notes] for DS, some configs are ignored
// config [imports] ->
// a) UseIndicesMeta (ignore),
// b) createTargetIndices
//  1) sourceIndex (ignore)
//  2) scaffolding (ignore)
func Test_without_scaffoldings_DS(t *testing.T) {
	fmt.Printf("\n** cli_use_cases_test.Test_without_scaffoldings_ds **\n")
	// a. setup
	es := cli.ES{
		Host:     os.Getenv(cli.ENV_HOST),
		Username: os.Getenv(cli.ENV_USERNAME),
		Password: os.Getenv(cli.ENV_PASSWORD),
	}
	imports := cli.Imports{
		BatchSize:          10000,
		UseIndicesMeta:     false, // should not affect anything as this flag would be ignored for DATASTREAM
		SourceFolder:       "./../../testdata/",
		CreateMissingIdxDS: true,
		CreateTargetIndices: []cli.Targets{
			{TargetIndex: "test_log_basic_dev", SourceFile: "log-basic-dev.data",
				SourceIndex: "NO_USE", Scaffolding: "NO-USE", Datastream: true},
		},
	}
	if !isESValidForTest(es) {
		fmt.Println(MSG_MISSING_CONNECTION_CONFIG)
		t.Skip()
	}
	c := http.DefaultClient
	c.Timeout = time.Duration(time.Duration.Minutes(1))

	// import ds (without source_idx and scaffoldings and useIndicesMeta (false) <- should ignore all these settings logically)
	if err := cli.ImportDataWithIndexCreation(&es, &imports); err != nil {
		t.Errorf("a. unexpected error on importing DS for [log-basic-dev], [%v]", err)
	}
	// verification
	if err := cli.CheckIfIndexExists(&es, "test_log_basic_dev", c); err != nil {
		t.Errorf("a-1. failed to check ds existence, [%v]", err)
	}
	// housekeep
	removeDSByCurl("test_log_basic_dev", &es)
}

// Test_without_scaffoldings_index - test importing for index without a scaffolding file.
// [notes] for index, some configs combinations have special handling
// config [imports] ->
// a) createTargetIndices.scaffolding ("") + CreateMissingIdxDS (true); => ignore index creation and let ES guess data type(s).
func Test_without_scaffoldings_index(t *testing.T) {
	fmt.Printf("\n** cli_use_cases_test.Test_without_scaffoldings_index **\n")
	// a. setup
	es := cli.ES{
		Host:     os.Getenv(cli.ENV_HOST),
		Username: os.Getenv(cli.ENV_USERNAME),
		Password: os.Getenv(cli.ENV_PASSWORD),
	}
	imports := cli.Imports{
		BatchSize:          10000,
		UseIndicesMeta:     true,
		SourceFolder:       "./../../testdata/",
		CreateMissingIdxDS: true,
		CreateTargetIndices: []cli.Targets{
			{TargetIndex: "test_simple_apache", SourceFile: "simplified_apache_demo.data",
				SourceIndex: "apach_demo", Scaffolding: "", Datastream: false},
		},
	}
	if !isESValidForTest(es) {
		fmt.Println(MSG_MISSING_CONNECTION_CONFIG)
		t.Skip()
	}
	c := http.DefaultClient
	c.Timeout = time.Duration(time.Duration.Minutes(1))
	// if simplified_apache_demo.data is missing...
	if err := prepareSimpleApacheDemoData("./../../testdata/simplified_apache_demo.data"); err != nil {
		if strings.Contains(err.Error(), "ERR1010") {
			fmt.Printf("[WARNING] source data file apach_demo.data is missing; hence skipping this test.\n")
			t.Skip()
		}
		t.Errorf("0. preparing stage error, [%v]", err)
	}
	// import ds (without source_idx and scaffoldings and useIndicesMeta (false) <- should ignore all these settings logically)
	if err := cli.ImportDataWithIndexCreation(&es, &imports); err != nil {
		t.Errorf("a. unexpected error on importing DS for [log-basic-dev], [%v]", err)
	}
	// verification
	if err := cli.CheckIfIndexExists(&es, "test_simple_apache", c); err != nil {
		t.Errorf("a-1. failed to check ds existence, [%v]", err)
	}
	// housekeep
	removeIndexByCurl("test_simple_apache", &es)
}

// Test_false_createMissingIndexDS_index - test behaviour based on importing index without
// create_index_datastream_if_missing set to true.
//
// create_index_datastream_if_missing (false - index)... -> should let es guess data schema (no error)
func Test_false_createMissingIndexDS_index(t *testing.T) {
	fmt.Printf("\n** cli_use_cases_test.Test_false_createMissingIndexDS_index **\n")
	// a. setup
	es := cli.ES{
		Host:     os.Getenv(cli.ENV_HOST),
		Username: os.Getenv(cli.ENV_USERNAME),
		Password: os.Getenv(cli.ENV_PASSWORD),
	}
	imports := cli.Imports{
		BatchSize:          10000,
		UseIndicesMeta:     true,
		SourceFolder:       "./../../testdata/",
		CreateMissingIdxDS: false,
		CreateTargetIndices: []cli.Targets{
			{TargetIndex: "test_simple_apache_2", SourceFile: "simplified_apache_demo.data",
				SourceIndex: "apach_demo", Datastream: false},
			// Scaffolding: "", (ignored for this flag if CreatedMissingIdxDS = false)
		},
	}
	if !isESValidForTest(es) {
		fmt.Println(MSG_MISSING_CONNECTION_CONFIG)
		t.Skip()
	}
	c := http.DefaultClient
	c.Timeout = time.Duration(time.Duration.Minutes(1))
	// if simplified_apache_demo.data is missing...
	if err := prepareSimpleApacheDemoData("./../../testdata/simplified_apache_demo.data"); err != nil {
		if strings.Contains(err.Error(), "ERR1010") {
			fmt.Printf("[WARNING] source data file apach_demo.data is missing; hence skipping this test.\n")
			t.Skip()
		}
		t.Errorf("0. preparing stage error, [%v]", err)
	}
	// import
	if err := cli.ImportDataWithIndexCreation(&es, &imports); err != nil {
		t.Errorf("a. failed to import data, [%v]", err)
	}
	// verification
	if err := cli.CheckIfIndexExists(&es, "test_simple_apache_2", c); err != nil {
		t.Errorf("a-1. expect test_simple_apache_2 available..., [%v]", err)
	}
	// housekeep
	removeIndexByCurl("test_simple_apache_2", &es)
}

// Test_false_createMissingIndexDS_ds
//
// create_index_datastream_if_missing (false - index)... ->
// this config flag is ignored since datastream required an index template to work (no matter what)
func Test_false_createMissingIndexDS_ds(t *testing.T) {
	fmt.Printf("\n** cli_use_cases_test.Test_false_createMissingIndexDS_ds **\n")
	// a. setup
	es := cli.ES{
		Host:     os.Getenv(cli.ENV_HOST),
		Username: os.Getenv(cli.ENV_USERNAME),
		Password: os.Getenv(cli.ENV_PASSWORD),
	}
	imports := cli.Imports{
		BatchSize:          10000,
		UseIndicesMeta:     true, // ignored for ds
		SourceFolder:       "./../../testdata/",
		CreateMissingIdxDS: false, // ignored (always true for DS) due to index_template MUST be created for DS to work.
		CreateTargetIndices: []cli.Targets{
			{TargetIndex: "test_simple_apache_3", SourceFile: "simplified_apache_demo.data",
				SourceIndex: "apach_demo", Datastream: true},
			// Scaffolding: "", (ignored for this flag if CreatedMissingIdxDS = false)
		},
	}
	if !isESValidForTest(es) {
		fmt.Println(MSG_MISSING_CONNECTION_CONFIG)
		t.Skip()
	}
	c := http.DefaultClient
	c.Timeout = time.Duration(time.Duration.Minutes(1))
	// if simplified_apache_demo.data is missing...
	if err := prepareSimpleApacheDemoData("./../../testdata/simplified_apache_demo.data"); err != nil {
		if strings.Contains(err.Error(), "ERR1010") {
			fmt.Printf("[WARNING] source data file apach_demo.data is missing; hence skipping this test.\n")
			t.Skip()
		}
		t.Errorf("0. preparing stage error, [%v]", err)
	}
	// import
	if err := cli.ImportDataWithIndexCreation(&es, &imports); err != nil {
		t.Errorf("a. failed to import data, [%v]", err)
	}
	// verification
	if err := cli.CheckIfIndexExists(&es, "test_simple_apache_3", c); err != nil {
		t.Errorf("a-1. expect test_simple_apache_3 available..., [%v]", err)
	}
	// housekeep
	removeDSByCurl("test_simple_apache_3", &es)
}

// test without target or source index... -> exception for sure as MANDATORY
func Test_wivout_source_or_target_index(t *testing.T) {
	fmt.Printf("\n** cli_use_cases_test.Test_wivout_source_or_target_index **\n")
	// a. setup
	es := cli.ES{
		Host:     os.Getenv(cli.ENV_HOST),
		Username: os.Getenv(cli.ENV_USERNAME),
		Password: os.Getenv(cli.ENV_PASSWORD),
	}
	imports := cli.Imports{
		BatchSize:          10000,
		UseIndicesMeta:     false,
		SourceFolder:       "./../../testdata/",
		CreateMissingIdxDS: false,
		CreateTargetIndices: []cli.Targets{
			{TargetIndex: "", SourceIndex: "apach_demo",
				SourceFile: "simplified_apache_demo.data", Datastream: false},
		},
	}
	if !isESValidForTest(es) {
		fmt.Println(MSG_MISSING_CONNECTION_CONFIG)
		t.Skip()
	}
	c := http.DefaultClient
	c.Timeout = time.Duration(time.Duration.Minutes(1))
	// import
	// missing target-index
	if err := cli.ImportDataWithIndexCreation(&es, &imports); err != nil {
		if !strings.Contains(err.Error(), "source and target index MUST be provided") {
			t.Errorf("a. expected error would be 'source and target index MUST be provided', actual [%v]", err)
		}
	}

	// missing source-index
	imports = cli.Imports{
		BatchSize:          10000,
		UseIndicesMeta:     false,
		SourceFolder:       "./../../testdata/",
		CreateMissingIdxDS: false,
		CreateTargetIndices: []cli.Targets{
			{TargetIndex: "target_apach_demo_1", SourceIndex: "",
				SourceFile: "simplified_apache_demo.data", Datastream: false},
		},
	}
	// import
	if err := cli.ImportDataWithIndexCreation(&es, &imports); err != nil {
		if !strings.Contains(err.Error(), "source and target index MUST be provided") {
			t.Errorf("b. expected error would be 'source and target index MUST be provided', actual [%v]", err)
		}
	}

	// missing both
	imports = cli.Imports{
		BatchSize:          10000,
		UseIndicesMeta:     false,
		SourceFolder:       "./../../testdata/",
		CreateMissingIdxDS: false,
		CreateTargetIndices: []cli.Targets{
			{TargetIndex: "", SourceIndex: "",
				SourceFile: "simplified_apache_demo.data", Datastream: false},
		},
	}
	// import
	if err := cli.ImportDataWithIndexCreation(&es, &imports); err != nil {
		if !strings.Contains(err.Error(), "source and target index MUST be provided") {
			t.Errorf("c. expected error would be 'source and target index MUST be provided', actual [%v]", err)
		}
	}

	// missing source-file...
	imports = cli.Imports{
		BatchSize:          10000,
		UseIndicesMeta:     false,
		SourceFolder:       "./../../testdata/",
		CreateMissingIdxDS: false,
		CreateTargetIndices: []cli.Targets{
			{TargetIndex: "target_apach_demo_1", SourceIndex: "apach_demo",
				SourceFile: "", Datastream: false},
		},
	}
	// import
	if err := cli.ImportDataWithIndexCreation(&es, &imports); err != nil {
		if !strings.Contains(err.Error(), "source DATA file MUST be provided") {
			t.Errorf("c. expected error would be 'source DATA file MUST be provided', actual [%v]", err)
		}
	}
}

// Test_duplication_of_meta_records_to_produce_err_log_file - test on importing twice on a particular set of data;
// if error exists would create a special errors.log file.
func Test_duplication_of_meta_records_to_produce_err_log_file(t *testing.T) {
	fmt.Printf("\n** cli_use_cases_test.Test_duplication_of_meta_records_to_produce_err_log_file **\n")
	// a. setup
	isDS := true
	es := cli.ES{
		Host:     os.Getenv(cli.ENV_HOST),
		Username: os.Getenv(cli.ENV_USERNAME),
		Password: os.Getenv(cli.ENV_PASSWORD),
	}
	imports := cli.Imports{
		BatchSize:          10000,
		UseIndicesMeta:     true,
		SourceFolder:       "./../../testdata/",
		CreateMissingIdxDS: true,
		CreateTargetIndices: []cli.Targets{
			{TargetIndex: "target_ap_demo_ds", SourceIndex: "apach_demo",
				SourceFile: "simplified_apache_demo.data", Datastream: isDS},
		},
	}
	if !isESValidForTest(es) {
		fmt.Println(MSG_MISSING_CONNECTION_CONFIG)
		t.Skip()
	}
	c := http.DefaultClient
	c.Timeout = time.Duration(time.Duration.Minutes(1))
	// first import
	if err := cli.ImportDataWithIndexCreation(&es, &imports); err != nil {
		t.Errorf("a. 1st import failed, [%v]", err)
	}
	// first verification
	if err := cli.CheckIfIndexExists(&es, "target_ap_demo_ds", c); err != nil {
		t.Errorf("a-1. 1st verification failed, [%v]", err)
	}

	// 2nd import - duplication of data
	if err := cli.ImportDataWithIndexCreation(&es, &imports); err != nil {
		t.Errorf("b. 1st import failed, [%v]", err)
	}
	// first verification
	if err := cli.CheckIfIndexExists(&es, "target_ap_demo_ds", c); err != nil {
		t.Errorf("b-1. 1st verification failed, [%v]", err)
	}
	// expect a file named errors.log under the source-folder
	if b, err := i.ReadFile("./../../testdata/errors.log"); err == nil {
		if len(b) <= 1 {
			t.Errorf("c-1. expect errors.log non empty..., [0] rows")
		}
		// housekeep
		removeFile("./../../testdata/errors.log")
	} else {
		// probably could not create an exception due to the import (might happen)
		t.Logf("c-2. no issues on re-importing; hence no errors.log file created...")
	}
	// housekeep
	if isDS {
		removeDSByCurl("target_ap_demo_ds", &es)
	} else {
		removeIndexByCurl("target_ap_demo_ds", &es)
	}
}

// prepareSimpleApacheDemoData - prepare the missing simplified_apache_demo.data
func prepareSimpleApacheDemoData(path string) (err error) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		// check whether the source... file exists or not as well
		if _, err2 := os.Stat("./../../testdata/apach_demo.data"); os.IsNotExist(err2) {
			return fmt.Errorf("[prepareSimpleApacheDemoData] ERR1010 the source file [apach_demo.data] does NOT exists, hence failed to prepare the simplified version...")
		}
		// read the 1st 10 lines of data from source file
		f, err2 := os.Open("./../../testdata/apach_demo.data")
		defer func() {
			if f != nil {
				f.Close()
			}
		}()
		if err2 != nil {
			return err2
		}
		scan := i.GetLineScanner(f)
		idx := 0
		s := bytes.NewBuffer(nil)
		for scan.Scan() {
			if idx >= 10 {
				break
			}
			s.WriteString(scan.Text())
			s.WriteString("\n")
			idx++
		}
		if len(s.Bytes()) > 0 {
			if err2 := i.WriteToFile(path, s.Bytes(), 0x755); err2 != nil {
				return err2
			}
		}
	}
	return
}

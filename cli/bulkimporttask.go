// Elastic Connector
// Copyright (C) 2021 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package cli

import (
	"bytes"
	"net/http"
	"sync"
)

// BulkImportTask - a Runner task implementation for WorkerPool.
type BulkImportTask struct {
	// lock - a lock / mutex for synchronous operations.
	lock sync.Mutex

	es           *ES
	imports      *Imports
	targetImport *Targets
	client       *http.Client

	errs bytes.Buffer
}

// NewBulkImportTask - create an instance of [BulkImportTask].
func NewBulkImportTask(es *ES, imports *Imports, targetImport *Targets, client *http.Client) (t *BulkImportTask) {
	t = new(BulkImportTask)
	t.lock = sync.Mutex{}
	t.es = es
	t.imports = imports
	t.targetImport = targetImport
	t.client = client
	t.errs = *bytes.NewBuffer(nil)

	return
}

func (t *BulkImportTask) Do(payload ...interface{}) (err error) {
	if len(payload) == 0 {
		return
	}
	if allPass, failureReason, err2 := runBulkImport(t.es, t.targetImport.TargetIndex, t.imports.SourceFolder, payload[0].(string), t.client); err2 != nil {
		return err2
	} else if !allPass {
		t.lock.Lock()
		//t.errs.WriteString(fmt.Sprintf("[ImportDataFromFile] some documents failed to be imported to [%v] in the batch\n", t.targetImport.TargetIndex))
		t.errs.WriteString(failureReason)
		t.lock.Unlock()
	}
	return
}

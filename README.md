# GLastic - connector for Elasticsearch (export and import data)

an Elasticsearch datasource connector project which handles data export and import functionalities (similar to bcp in MSSQL). Despite of snapshot backups, __GLastic__ provides another approach to make a copy of your indices data which is essential for data integration between systems. Take an example, shopping transactions would need to be exported and integrate with Accounting systems for analysis and reporting purposes; since Elasticsearch snapshot could only restore the target dataset into a cluster, hence there is still a need to extract these data by some ways.

# export data (index or data-stream)
<pre>./glastic -c {{config_file.json}} export</pre>

# import data (index or data-stream)
<pre>./glastic -c {{config_file.json}} import</pre>

---

# configuration (connectivity related)
<pre>{
 "es_host": "http://localhost:9200",
 "es_username": "elastic",
 "es_password": "password"
}</pre>

### connection config

| key       | meaning |
| ----------- | ----------- |
|  es_host*      | the host name of the Elasticsearch node for connection |
|  es_username      | username for authentication (if required)       |
|  es_password   | password for authentication (if required)        |

Another way to configure the connectivity is through setting __environment variables__ as follows:

| env var name       | meaning |
| ----------- | ----------- |
|  GLastic_HOST*      | the host name of the Elasticsearch node for connection |
|  GLastic_USER      | username for authentication (if required)       |
|  GLastic_PWD   | password for authentication (if required)        |

---

# configuration (export)
<pre>{
   "exports": {
      "batch_size": 10000,
      "export_indices_meta": true,
      "export_indices_settings_mappings": true,
      "indices": [ "apache_demo_backup", "apach_demo" ],
      "filter_query": "{\"bool\": {\"should\": [{\"match\": {\"message\": \"log file png\"}},{\"match\": {\"request\": \"log file png\"}}]}}",
      "target_folder": "./testdata/"
   }</pre>

### exports config

| key       | meaning |
| ----------- | ----------- |
| batch_size      | size of query page (max is 10000), set to __10000__ if not provided |
| export_indices_meta      |  export meta (_index, _type, _id), default is __false__      |
| export_indices_settings_mappings   | export mappings + settings, default is __false__        |
| indices | the set of indices for export, default is __[]__ and simply do nothing |
| filter_query | query filter to be applied, default is __""__ which is equivalent to a "__match_all__" query |
| target_folder | the target folder for storing the exported data, default is __./__ which is equivalent to the current folder |

_Legend_

'*' denoted as a required config

--- 

### when to exclude certain configurations

| scenario | general export  |
| ------ | ----- |
| ignorable config(s) | __ALL__ except __indices__. Target indices for export SHOULD be provided or else nothing would be exported.  |

| scenario | expect to be able to _CLONE_ the dataset for future imports   |
| ------ | ----- |
| ignorable config(s) | __ALL__ except __indices__, __export_indices_meta__ and __export_indices_settings_mappings__. For clone purpose, mappings+settings, meta data (_index, _type, _id) MUST be available in the exported file.  |

| scenario | export subsets of the data  |
| ------ | ----- |
| ignorable config(s) | __ALL__ except __indices__ and __filter_query__. Subsets require a query to filter out the wanted documents.  |

| scenario | export data to a dedicated location  |
| ------ | ----- |
| ignorable config(s) | __ALL__ except __indices__ and __target_folder__.  |

---

# configuration (import)
<pre>{
   "imports": {
      "batch_size": 20000,
      "create_target_indices": [ 
        {
            "target_index": "apache_demo_copy",
            "source_index": "apache_demo_backup",
            "source_file": "apache_demo_backup.data",
            "scaffolding": "apache_demo_backup-settings-mappings.data",
            "data_stream": false
        },
        {
            "target_index": "apache_demo_copy_dstream",
            "source_index": "apach_demo",
            "source_file": "apach_demo.data",
            "scaffolding": "apach_demo-settings-mappings.data",
            "data_stream": true
        }
      ],
      "create_index_datastream_if_missing": true,
      "source_folder": "./testdata/", 
      "use_indices_meta": false
  }</pre>

### imports config

| key       | meaning |
| ----------- | ----------- |
| batch_size      | size of documents to be imported per batch, default is __10000__ |
| create_index_datastream_if_missing      | create missing index / data-stream, default is __true__ |
| source_folder | where the data files are located, default is __./__ which is equivalent to current folder |
| use_indices_meta | apply meta data (_index, _type, _id), default is __false__ |

### imports - create_target_indices config

| key       | meaning |
| ----------- | ----------- |
| target_index*      | index / data-stream name to be imported |
| source_index*      | index / data-stream name the data originates from |
| source_file*      | data file name, the file name would be appended after the _source_folder_'s value |
| scaffolding      | mappings+settings file name, the file name would be appended after the _source_folder_'s value |
| data_stream*      | importing a data-stream set the value to __true__, for importing an index set the value to __false__ |

_Legend_

'*' denoted as a required config

--- 
### import scenarios

| scenario | general import  |
| ------ | ----- |
| ignorable config(s) | __ALL__ except __create_target_indices__. __create_target_indices__ is the essential information for imports and MUST be provided or else nothing would be done.  |

| scenario | general import (data-stream)  |
| ------ | ----- |
| ignorable config(s) | __ALL__ except __create_target_indices__; within __create_target_indices__ the configs -  __scaffolding__ is ignored AND __data_stream__ MUST be set to __true__.  |

| scenario | general import (index) and let Elasticsearch auto configure the mappings  |
| ------ | ----- |
| ignorable config(s) | __ALL__ except __create_target_indices__; within __create_target_indices__ the configs - __scaffolding__ SHOULD not be set at all. Also __data_stream__ MUST be set to __false__.  |

| scenario | general import (index) and expect original mappings applied  |
| ------ | ----- |
| ignorable config(s) | __ALL__ except __create_target_indices__; within __create_target_indices__ the configs - __scaffolding__ file SHOULD be set here. Also __data_stream__ MUST be set to __false__.  |

| scenario | general import (index) as a CLONE  |
| ------ | ----- |
| ignorable config(s) | __ALL__ except __create_target_indices__; within __create_target_indices__ the configs - __scaffolding__ file SHOULD be set here AND __use_indices_meta__ SHOULD set to __true__. Also __data_stream__ MUST be set to __false__.  |

---

# docker 
GLastic is also available as a Docker image, simply run the following:
<pre>docker pull quemaster/glastic</pre>

### locations

| location | description  |
| ------ | ----- |
| /usr/bin/glastic | the binary executable is available under /usr/bin; hence it is accessible everywhere within the image |
| /glastic/elasticconnector | the source code and unit test config files |

<br><br>

### mounting a local volume / drive to the docker container
<pre>docker run -it --mount src={local_folder},target={folder_within_the_container},type=bind quemaster/glastic</pre>

Configuration files and exported data files could be stored and accessible in the {local_folder} in your host machine (e.g. on your Windows or Mac).

---

# behind the scenes
## export
The export feature heavily relies on the [search_after](https://www.elastic.co/guide/en/elasticsearch/reference/7.15/paginate-search-results.html#search-after).

__PS__. In earlier Elasticsearch releases, the approach for iterating through the dataset (indices) would be using the [scroll](https://www.elastic.co/guide/en/elasticsearch/reference/7.15/paginate-search-results.html#scroll-search-results) but starting from 7.x the official documentation recommends to choose __search_after__.

The downside of __search_after__ is index-state might not be preserved unless a __PIT__ id is provided, in which for normal export operations PIT id(s) are not available. For more information about PIT, do read (https://www.elastic.co/guide/en/elasticsearch/reference/7.15/point-in-time-api.html).

## import
The import feature relies on the [bulk](https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-bulk.html) api. Import is unexpectedly more complicated than export. Starting from 7.x, data could be stored in either
- data index OR
- data stream

Hence handling on the import varies. __GLastic__ supports import of both types, but for data-streams there were some assumptions as follows:
- based on the nature of data-streams, the data should be "created" (or simply appended) hence all meta data (i.e. _index, _type and _id) from the exported file would be ignored.
- a dedicated index-template is required per data-stream (at the moment of time); for simplicity, the index-template created through the import operations would simply ignore all mappings and settings. The reason is throughout time, the in-production index-templates might be modified and hence different backing-index associated with the data-stream might have different mappings and settings. To avoid misconfiguration on the import operation, __GLastic__ just create a bare-bone index-template and make it work.

__PS__. for data-streams, it is suggested that the associated index-template SHOULD be created manually with the designated mappings and settings correctly configured; then feel free to use __GLastic__ to import data into the target data-stream by setting the _"__create_index_datastream_if_missing__"_ config to __false__.


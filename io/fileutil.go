// Elastic Connector
// Copyright (C) 2021 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package io

import (
	"bufio"
	"io/fs"
	"io/ioutil"
	"os"
)

// WriteToFile - write the provided [content] to the [filepath].
func WriteToFile(filepath string, content []byte, perm fs.FileMode) (err error) {
	return ioutil.WriteFile(filepath, content, perm)
}

// ReadFile - read the provided [filepath] content.
func ReadFile(filepath string) (content []byte, err error) {
	return ioutil.ReadFile(filepath)
}

// GetLineScanner - returns a scanner for reading line by line info. To iterate, call the scanner.Scan() api.
func GetLineScanner(fHandle *os.File) (scanner bufio.Scanner) {
	scanner = *bufio.NewScanner(fHandle)
	scanner.Split(bufio.ScanLines)
	return
}

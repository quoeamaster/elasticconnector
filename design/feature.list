[brainstorm]

{export}
- able to export target indices' data in per-line json format { data_01 ... }\n{ data_02 ... }
- optionally export the target indices' settings and mappings in per-line json format as well { index_a ... }\n{ index_b ... }
- able to apply a query filter instead of exporting everything
  - through CLI flag --query OR
  - through a config file setting. (TBD)
- optionally meta data could be ignored (i.e. _index, _id, _type)
  - by default all meta info are exported as well which creates a bigger payload.
  - if the exported data are not expected to re-use the meta during import; then ignoring them would be more efficient.

{import}
= optionally able to create the target indices based on supplied settings + mappings info 
  = (info in per-line json format)
= able to import supplied data into the target indices
  = data MUST be in per-line json format {data_01...}\n{data_02...}
  = no filtering is supported, again makes sense since there is no simple way to identify which documents should be imported... quite tedious  
  = able to import the data based on the meta data involved OR just create as a brand new record (could have data duplication issue)
  = [deprecated] only 1 target index is supported (makes sense since there is no simple way to identify which index a document belongs to... quite tedious)
= mutli-thread / worker / routine for importing
= error logging during import (stored at {{sourceFolder_config}}/errors.log)

{extras}

-=-=-=-=-=-=-=-=-=

[queryDSL samples]

{export}
[get index setting + mapping]
GET test_app_metric
...
{
  "test_app_metric" : {
    "aliases" : { ... },
    "mappings" : { ... },
    "settings" : {
      "index" : {
        "routing" : {
          "allocation" : {
            "include" : {
              "_tier_preference" : "data_content"
            }
          }
        },
        "number_of_shards" : "1",
        "number_of_replicas" : "1",

        // ignored the following...
        "provided_name" : "test_app_metric",
        "creation_date" : "1631103637987",
        "uuid" : "H1PG7CNnRPuQld-WzyM2Dw",
        "version" : {
          "created" : "7130099"
        }
      }
    }
  }
}

[get data for export]
-> search_after and size: 10000 is required to make sure all data are iteratable.

GET test_app_metric/_search
{
  "size": 10000,
  # this part is apply-able, by default is a "match_all" query.
  "query": {
    "match": {
      "country.keyword": "PT"
    }
  },
  "search_after": [1],
  "sort": [
    {
      "_doc": {
        "order": "asc"
      }
    }
  ]
}
... output ...
{
"took" : 2,
"timed_out" : false,
"_shards" : {
  "total" : 1,
  "successful" : 1,
  "skipped" : 0,
  "failed" : 0
},
"hits" : {
  "total" : {
    "value" : 3,
    "relation" : "eq"
  },
  "max_score" : null,
  "hits" : [
    {
      "_index" : "aflo_flow_config_dev",
      "_type" : "_doc",
      "_id" : "g0Xl3HcBKm7HulAc_Olg",
      "_score" : null,
      "_source" : {
        "biz_account_id" : "wecode",
        "flow_id" : "f0001-d",
... }}}}
or ERROR ~~~
{
  "error" : {
    "root_cause" : [
      {
        "type" : "parsing_exception",
        "reason" : "Unknown key for a VALUE_STRING in [w].",
        "line" : 2,
        "col" : 8
      }
    ],
    "type" : "parsing_exception",
    "reason" : "Unknown key for a VALUE_STRING in [w].",
    "line" : 2,
    "col" : 8
  },
  "status" : 400
}

{import}
[create a target index with settings and mappings]
PUT test_app_metric_1
{
  "settings": { ... },
  "mappings": { ... }
}

[import / index data]

POST _bulk 
{ "index": { "_index": "kibana_sample_data_flights", "_type": "_doc", "_id": "kdjfkur_1jfu1" } }
{ "message": "hi", "age": 13 }
... error
{
  "took" : 351,
  "errors" : true,
  "items" : [
    {
      "index" : {
        "_index" : "test_bulk",
        "_type" : "_doc",
        "_id" : "OjC0-HsBugmWpr_4hQ9E",
        "_version" : 1,
        "result" : "created",
        "_shards" : {
          "total" : 2,
          "successful" : 2,
          "failed" : 0
        },
        "_seq_no" : 0,
        "_primary_term" : 1,
        "status" : 201
      }
    },
    {
      "update" : {
        "_index" : "test_bulk",
        "_type" : "_doc",
        "_id" : "123",
        "status" : 404,
        "error" : {
          "type" : "document_missing_exception",
          "reason" : "[_doc][123]: document missing",
          "index_uuid" : "i0QnPYNFR8uvsz1zYNV5MQ",
          "shard" : "0",
          "index" : "test_bulk"
        }
      }
    }
  ]
}


-> if meta data is available and not configured to ignore them.
PUT test_app_metric_1/_doc/{_id}
{
   ...
}
OR
-> configured to ignore meta; use the supplied _id.
POST test_app_metric_1/_doc/
{
   ...
}

